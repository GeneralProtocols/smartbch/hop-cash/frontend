// Import Define Plugin so that we can inject ENV variables.
const { DefinePlugin } = require('webpack');

// Import plugin to split CSS into files.
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

// Import plugins to remove unnecessary momentjs data.
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');

// Import plugin to handle relative paths.
const path = require('path');

module.exports =
{
	// Add your application's scripts below
	entry:
	{
		index:
		[
			'./source/index.html',
			'./source/index.ts',
			'./source/index.css',
			'./source/images/logo.png',
			'./source/images/logo-dark.png',
			'./source/images/favicon.png',
			'./source/images/bitcoincash.png',
			'./source/images/metamask.png',
			'./source/images/smartbch.png',
			'./source/images/twitter.png',
			'./source/images/telegram.png',
			'./source/images/questionmark.png',
			'./source/images/status_none.png',
			'./source/images/status_pending.png',
			'./source/images/status_complete.png',
			'./source/images/status_insufficient_liquidity.png',
			'./source/images/status_failed.png',
		],
	},
	mode: 'production',
	devtool: 'inline-source-map',
	output:
	{
		path: path.resolve(__dirname, 'public/'),

		filename: '[name].js',
		clean: true,
	},
	module:
	{
		rules:
		[
			{
				// Only run `.ts` files through the the webpack and plugins
				test: /\.ts$/,
				use: 'ts-loader',

				// Do not parse node modules.
				exclude:
				[
					/node_modules/
				],

				// But do include source maps for node modules.
				// enforce: 'pre',
				// use: ['source-map-loader'],
			},
			{
				test: /\.css$/,
				use:
				[
					MiniCssExtractPlugin.loader,
					"css-loader",
				],
			},
			{
				test: /\.(png|jpg|jpeg|gif)$/i,
				type: 'asset/resource',
				generator:
				{
					filename: '[name][ext]',
				}
			},
			{
				test: /\.webmanifest$/,
				type: 'asset/resource',
				generator:
				{
					filename: '[name][ext]',
				}
			},
			{
				// ...
				test: /\.html$/i,
				type: 'asset/resource',
				generator:
				{
					filename: '[name][ext]',
				}
			},
			{
				test: /\.html$/i,
				type: 'asset/resource',
				generator:
				{
					filename: '[name][ext]',
				}
			},
		]
	},
	plugins:
	[
		new MomentLocalesPlugin({ localesToKeep: ['en'] }),
		new MiniCssExtractPlugin({ filename: '[name].css' }),
		new DefinePlugin({
			// Allow the Liquidity/Receiving Addresses to be set from ENV variables at build-time.
			// NOTE: Webpack quirk that we must JSON.stringify our process.env variables.
			'process.env.CASH_RECEIVING_ADDRESS': JSON.stringify(process.env.CASH_RECEIVING_ADDRESS),
			'process.env.CASH_LIQUIDITY_ADDRESS': JSON.stringify(process.env.CASH_LIQUIDITY_ADDRESS),
			'process.env.SMART_RECEIVING_ADDRESS': JSON.stringify(process.env.SMART_RECEIVING_ADDRESS),
			'process.env.SMART_LIQUIDITY_ADDRESS': JSON.stringify(process.env.SMART_LIQUIDITY_ADDRESS)
		})
	],
	resolve:
	{
		extensions: ['.js', '.jsx', '.ts', '.json'],
		fallback:
		{
			// Electrum-cash requires network and event related polyfills.
			events: require.resolve("events/"),
			net: false,
			tls: false,
		}
	},
}
