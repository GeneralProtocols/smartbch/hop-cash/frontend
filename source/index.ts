// Import our storage functionality.
// NOTE: This will attempt to use IndexedDB but fallback to an in-memory store if not available.
import { get, set, del } from './scripts/storage';

// Import library to communicate with the Bitcoin Cash chain/network.
import { ElectrumClient } from 'electrum-cash';
import { electrumServers } from 'electrum-cash-servers';

// Import library to manage bitcoin cash data structures.
import { hexToBin, binToHex, instantiateSha256, instantiateSecp256k1, instantiateRipemd160, encodePrivateKeyWif, CashAddressType, encodeCashAddress, encodeBase58Address } from '@bitauth/libauth';

// Import library to communicate with the SmartBCH chain/network.
import { ethers } from 'ethers';
import MetaMaskOnboarding from '@metamask/onboarding';

// Import Hop.Cash Constants, Electrum Calls and Functions.
import {
	FEE_RATE_MULTIPLIER, MINIMUM_FEE_SATOSHIS, MINIMUM_FRONTEND_SATOSHIS, SATOSHIS_PER_BITCOIN_CASH, CASH_TO_SMART, SMART_TO_CASH, CASH_REQUIRES_ONE_CONFIRMATIONS_THRESHOLD, CASH_REQUIRES_TWO_CONFIRMATIONS_THRESHOLD, CASH_SATOSHI_DUST_LIMIT, SMART_CHAIN_ID, SMART_CHAIN_ID_HEX, SMART_CHAIN_PARAMETERS, SMART_CONTRACT_ABI, SMART_CONTRACT_ADDRESS,
	broadcastTransaction, getBalance, getCurrentHeight, getTransaction, getUnspentOutputs, getTransactionHeight,
	createCashBridgeTransaction, deriveHopWalletPrivateKey, getRequiredCashConfirmations, isValidCashAddress, isValidSmartAddress, parseCashPayoutTransaction,
} from '@generalprotocols/hop-cash';

import
{
	introductionForm,
} from './scripts/ui/introduction';

import
{
	// Elements
	proposalForm,
	proposalDirection,
	proposedBridgeAmount,
	proposedPayoutAddress,
	proposalMinBridgeAmount,
	proposalMaxBridgeAmount,

	// Functions
	clearProposalForm,
	disableProposalButton,
	showDirectionToCash,
	showDirectionToSmart,
	proposedDirectionIsToCash,
	proposedDirectionIsToSmart,
	showAndLimitMaxBridgeAmount,
	setProposedAmountToMinimum,
	setProposedAmountToMaximum,
	setProposedPayoutAddress,
	updateBridgeButtonAmount,
	updateBridgeButtonTimeEstimate,
	getProposedBridgeAmount,
	updateProposalBridgeAmountValidity,
	getProposedPayoutAddress,
	validateProposedBridgePayoutAddress,
	invalidateProposedBridgePayoutAddress,
	updateProposalButtonValidity,
} from './scripts/ui/proposal';

import
{
	// Elements
	paymentForm,
	paymentAmount,
	paymentAmountInput,
	paymentCashAddress,
	paymentCashAddressInput,
	paymentLegacyAddress,
	paymentLegacyAddressInput,

	// Functions
	clearPaymentForm,
	updatePaymentDetails,
	updatePaymentAmountReceived,
	disablePaymentButton,
	showPaymentBelowMinimumAmountNotice,
	hidePaymentBelowMinimumAmountNotice,
} from './scripts/ui/payment';

import
{
	// Elements
	requestTransactionToBridge,
	requestTransactionToBridgeInput,
	requestTransactionFromBridge,
	requestTransactionFromBridgeInput,
	requestCompleteButton,
	promotionLink,

	// Functions
	clearRequestForm,
	setRequestAmountReceived,
	setRequestBridgeTransaction,
	setRequestPayoutTransaction,
	setBridgeRequirement,
	updateRequestStatus,
	enableRequestButton,
	disableRequestButton,
} from './scripts/ui/request';

import
{
	// Elements
	bridgeMinimumAmountElements,
	feePercentageElements,
	minimumFeeElements,
	installWallet,
	connectWallet,
	unlockWallet,
	setupNetwork,
	helpToggle,

	// Functions
	enableLayoutDebugging,
	showProposalScreen,
	showPaymentScreen,
	showRequestScreen,
	showRequirementsOverlay,
	hideRequirementsOverlay,
	showIntroduction,
	hideIntroduction,
} from './scripts/ui/misc';

import {
	// Functions
	initializeTranslationSupport,
} from './scripts/ui/language';

interface HopWallet
{
	privateKey: Uint8Array;
	privateKeyWIF: string;
	publicKey: Uint8Array;
	cashAddress: string;
	legacyAddress: string;
}

interface BridgeRequest
{
	bridgeDirection: number;
	bridgeAmount: number;
	payoutAddress: string;
	payoutTransactionHash?: string;
	sourceTransactionHash?: string;
}

class HopCashFrontend
{
	// Set up a temporary list of known bridge requests.
	userBridgeRequests: Map<string, string> = new Map();

	// Set up a temporary list of known transactions.
	userParsedTransactions: Array<string> = [];

	// Set up a local list of signatures for each metamask account.
	// NOTE: These are used to derive a temporary in-browser wallet.
	userAccountSignatures: Map<string, string> = new Map();

	// Stores the current bridge request when waiting for payment or settlement.
	bridgeRequest?: BridgeRequest;

	// The hop-cash bridge addresses on the bitcoin cash side.
	cashReceivingAddress: string = process.env.CASH_RECEIVING_ADDRESS ?? 'bitcoincash:qqa0dj5rwaw2s4tz88m3xmcpjyzry356gglq7zvu80';
	cashLiquidityAddress: string = process.env.CASH_LIQUIDITY_ADDRESS ?? 'bitcoincash:qzteyuny2hdvvcd4tu6dktwx9f04jarzkyt57qel0y';

	// The currently available liquidity for paying out on the bitcoin cash side.
	cashLiquidityValue: number = 0.01;

	// For consistency, we will call the bitcoin cash network connection in similar fashion as the smartBCH provider.
	cashProvider: ElectrumClient;

	// The hop-cash bridge addresses on the smartbch side.
	smartReceivingAddress: string = process.env.SMART_RECEIVING_ADDRESS ?? '0x3207d65b4D45CF617253467625AF6C1b687F720b';
	smartLiquidityAddress: string = process.env.SMART_LIQUIDITY_ADDRESS ?? '0xa659c0434399a8D0e15b8286b39f8d97830F8F91';

	// The currently available liquidity for paying out on the smartbch side.
	smartLiquidityValue: number = 0.01;

	// The currently selected accounts address and balance
	smartAccountAddress: string;
	smartAccountBalance: number;

	// The amount of value to reserve from the user balance to pay for gas fees.
	smartGasFeeEstimate: number = 0.00005;

	// A Contract object is an abstraction of a contract (EVM bytecode) deployed on the Ethereum network.
	// It allows for a simple way to serialize calls and transactions to an on-chain contract and deserialize their results and emitted logs.
	smartContract: ethers.Contract;

	// A Provider is an abstraction of a connection to the Ethereum network, providing a
	// concise, consistent interface to standard Ethereum node functionality.
	smartProvider: ethers.providers.Provider;

	// A Signer in ethers is an abstraction of an Ethereum Account, which can be used to sign messages and transactions and
	// send signed transactions to the Ethereum Network to execute state changing operations.
	smartSigner: ethers.Wallet;

	// Configure how many percent of the liquidity pool to show as maximum values.
	poolLiquidityPercent = 50;

	// State variable to track whether the user's wallet is in the process of sending a Transaction.
	// NOTE: We need this because some wallets (e.g. ImToken) will not resolve the sendTransaction RPC call until the transaction is confirmed.
	//       So this is used to ensure that other events do not enable the Bridge Button when this flag is true.
	isWaitingForWalletTransaction = false;

	constructor()
	{
		// Load the frontend once the web page structure has been recognized.
		document.addEventListener('DOMContentLoaded', this.initializeFrontend.bind(this));
	}

	/**
	 * Inject our Hop.Cash constants into the relevant DOM Objects in our HTML.
	 */
	async initializeFrontendConstants(): Promise<void>
	{
		// Calculate the Bridge Minimum Amount in Cash Units.
		// NOTE: If this value changes, we may need to change the precision.
		const bridgeMinimumInCashUnits = (MINIMUM_FRONTEND_SATOSHIS / SATOSHIS_PER_BITCOIN_CASH).toPrecision(1);

		// Inject the Bridge Minimum Amount into relevant functional elements.
		proposalMinBridgeAmount.innerText = bridgeMinimumInCashUnits;
		proposedBridgeAmount.setAttribute('min', bridgeMinimumInCashUnits);

		// Inject the Bridge Minimum Amount into relevant display elements.
		for(const elementIndex in bridgeMinimumAmountElements)
		{
			bridgeMinimumAmountElements[elementIndex].innerText = bridgeMinimumInCashUnits;
		}

		// Calculate the fee percentage based on constant.
		// NOTE: If this value changes, we may need to change the precision.
		const feePercentage = (FEE_RATE_MULTIPLIER * 100).toPrecision(1);

		// Inject the Fee Percentage into relevant display elements.
		for(const elementIndex in feePercentageElements)
		{
			feePercentageElements[elementIndex].innerText = `${feePercentage}%`;
		}

		// Calculate the absolute minimum fee based on constant.
		// NOTE: If this value changes, we may need to change the precision.
		const minimumFee = (MINIMUM_FEE_SATOSHIS / SATOSHIS_PER_BITCOIN_CASH).toPrecision(1);

		// Inject the Fee Percentage into relevant display elements.
		for(const elementIndex in minimumFeeElements)
		{
			minimumFeeElements[elementIndex].innerText = minimumFee;
		}
	}

	async initializeBitcoinCash(): Promise<void>
	{
		// Set the transport to use encrypted websockets.
		const transportScheme = 'wss';

		// Initialize an electrum cluster that trusts the servers it connects to.
		this.cashProvider = new ElectrumClient('HopCashFrontend', '1.4.3', electrumServers[0].host, electrumServers[0].transports[transportScheme], transportScheme);

		await this.cashProvider.connect();

		/*
		// Add the default servers to the cluster.
		for(const server of electrumServers)
		{
			try
			{
				//await this.cashProvider.addServer(server.host, server.transports[transportScheme], transportScheme);
			}
			catch(error: any)
			{
				// Do nothing, as we will verify if we have enough connections with the ready() call before finalizing
				// NOTE: The browsers console log may still show the network errors.
			}
		}

		// If we cannot reach enough connections to provide service, throw an error.
		if(!await this.cashProvider.ready())
		{
			throw(new Error(`Failed to initialize Bitcoin Cash networking: no servers were reachable.`));
		}
		*/

		// Monitor the liquidity on the bitcoin cash pool address.
		await this.cashProvider.subscribe(this.updateCashPoolBalance.bind(this), 'blockchain.address.subscribe', this.cashLiquidityAddress);

		// Update user related history when the bitcoin cash pool address have had activity.
		await this.cashProvider.subscribe(this.updateUserAddressHistory.bind(this), 'blockchain.address.subscribe', this.cashLiquidityAddress);

		// Update the number of confirmations for the user's Cash Bridge Transaction.
		// NOTE: We subscribe here to ensure that we only end up with one listener.
		//       The callback will simply return if there is no Cash to Smart Bridge Request pending.
		await this.cashProvider.subscribe(this.updateCashConfirmations.bind(this), 'blockchain.headers.subscribe');
	}

	/**
	 * Derives a bitcoin cash keypair and related addresses from a signature string.
	 *
	 * @param signature   string containing the unique signature from the user.
	 *
	 * @returns HopWallet object with the privateKey, publicKey, cashAddress and legacyAddress properties.
	 */
	async deriveWalletFromSignature(signature: string): Promise<HopWallet>
	{
		// Instantiate Libauth crypto interfaces
		const secp256k1 = await instantiateSecp256k1();
		const sha256 = await instantiateSha256();
		const ripemd160 = await instantiateRipemd160();

		// Derive the Private Key for the intermediary frontend Wallet ("Hop Wallet").
		// NOTE: This function derives a key that is incompatible with Hop.Cash V1.
		//       If V1 derivation is needed, deriveLegacyHopWalletPrivateKey() can be used.
		const privateKey = await deriveHopWalletPrivateKey(signature);

		// Make sure the Private Key is 32 bytes.
		// NOTE: It should not be possible to derive a key that is not 32 bytes.
		//       But we will err on the side of caution and make absolutely sure.
		if(privateKey.length !== 32)
		{
			throw(new Error(`Private Key must be 32 bytes (Private Key is ${privateKey.length} bytes)`));
		}

		// Encode the Private Key entropy into WIF format.
		const privateKeyWIF = encodePrivateKeyWif(sha256, privateKey, 'mainnet');

		// Derive the corresponding public key.
		const publicKey = secp256k1.derivePublicKeyCompressed(privateKey);

		// Hash the public key hash according to the P2PKH scheme.
		const publicKeyHash = ripemd160.hash(sha256.hash(publicKey));

		// Encode the public key hash into a P2PKH cash address.
		const cashAddress = encodeCashAddress('bitcoincash', CashAddressType.P2PKH, publicKeyHash);
		const legacyAddress = encodeBase58Address(sha256, 'p2pkh', publicKeyHash);

		// Return the derived wallet information.
		return { privateKey, privateKeyWIF, publicKey, cashAddress, legacyAddress };
	}

	async estimateBridgeSpeed(): Promise<void>
	{
		const bridgeAmount = await getProposedBridgeAmount();

		// TODO: Use internal representations here.
		const minBridgeAmount = Number(proposedBridgeAmount.getAttribute('min')!);
		const maxBridgeAmount = Number(proposedBridgeAmount.getAttribute('max')!);

		if(bridgeAmount < minBridgeAmount)
		{
			await updateBridgeButtonTimeEstimate('CANNOT_BRIDGE_LESS_THAN_MINIMUM_AMOUNT');
		}
		else if(bridgeAmount > maxBridgeAmount)
		{
			await updateBridgeButtonTimeEstimate('CANNOT_BRIDGE_MORE_THAN_MAXIMUM_AMOUNT');
		}
		// Toggle based on current direction..
		else if(await proposedDirectionIsToCash())
		{
			await updateBridgeButtonTimeEstimate('TIME_ESTIMATE_TO_CASH');
		}
		else if(bridgeAmount < CASH_REQUIRES_ONE_CONFIRMATIONS_THRESHOLD)
		{
			await updateBridgeButtonTimeEstimate('TIME_ESTIMATE_TO_SMART_REQUIRES_DSP');
		}
		else if(bridgeAmount < CASH_REQUIRES_TWO_CONFIRMATIONS_THRESHOLD)
		{
			await updateBridgeButtonTimeEstimate('TIME_ESTIMATE_TO_SMART_REQUIRES_ONE_CONFIRMATIONS');
		}
		else
		{
			await updateBridgeButtonTimeEstimate('TIME_ESTIMATE_TO_SMART_REQUIRES_TWO_CONFIRMATIONS');
		}
	}

	async toggleBridgeDirection(): Promise<void>
	{
		// Toggle based on current direction..
		if(await proposedDirectionIsToCash())
		{
			await showDirectionToSmart(this.smartAccountAddress);
		}
		else
		{
			await showDirectionToCash();
		}

		// Update the bridge speed estimation.
		await this.estimateBridgeSpeed();

		// Update the max amount presentation based on the new direction.
		await this.updateMaxAmount();
	}

	async initializeBridgeDirectionToggle(): Promise<void>
	{
		// Toggle bridge direction when clicked.
		proposalDirection.addEventListener('click', this.toggleBridgeDirection.bind(this), false);
	}

	async initializeHelpToggle(): Promise<void>
	{
		// Toggle bridge direction when clicked.
		helpToggle.addEventListener('click', showIntroduction, false);
	}

	async initializePromotionLink(): Promise<void>
	{
		// Toggle bridge direction when clicked.
		promotionLink.addEventListener('click', showIntroduction, false);
	}

	async updateCashPoolBalance(): Promise<void>
	{
		// Get Cash Liquidity Address balance.
		const balance = await getBalance(this.cashProvider, this.cashLiquidityAddress);

		// Convert to BCH Units.
		this.cashLiquidityValue = balance / SATOSHIS_PER_BITCOIN_CASH;

		// Update the max amount in the UI.
		await this.updateMaxAmount();

		// Update the UI to indicate whether there is sufficient liquidity to complete the Pending Request (if there is one).
		await this.updatePendingRequestLiquidityStatus();
	}

	async determineMaxBridgeAmount(): Promise<number>
	{
		if(await proposedDirectionIsToCash())
		{
			// Consider the target liquidity percent when determining what the pool recommends.
			const maxPoolDisplayed = this.cashLiquidityValue * (this.poolLiquidityPercent / 100);

			// Consider the estimated sending gas fee when determining the user max amount.
			const maxUserCanSend = this.smartAccountBalance - this.smartGasFeeEstimate;

			// Use the lowest balance between the pool and users wallet, capped to zero so we don't give negative numbers to our users.
			const maxPossibleBridgeAmount = Math.max(0, Math.min(maxUserCanSend, maxPoolDisplayed));

			// Return the recommended max amount, capped by the users funds.
			return maxPossibleBridgeAmount;
		}

		// Consider the target liquidity percent when determining what the pool recommends.
		const maxPossibleBridgeAmount = (this.smartLiquidityValue * (this.poolLiquidityPercent / 100));

		// Return the recommended max amount for the bridge pool.
		return maxPossibleBridgeAmount;
	}

	async updateMaxAmount(): Promise<void>
	{
		// Determine what limits we should apply for the max bridge amount.
		const maxBridgeAmount = await this.determineMaxBridgeAmount();

		// Update the visual display and limit input values for the user.
		await showAndLimitMaxBridgeAmount(maxBridgeAmount);

		// Validate the current input data now that we have changed the limits.
		await this.validateBridgeRequest();
	}

	async enterMinAmount(): Promise<void>
	{
		await setProposedAmountToMinimum();

		await this.updateBridgeAmount();
		await this.validateBridgeRequest();
	}

	async enterMaxAmount(): Promise<void>
	{
		await setProposedAmountToMaximum();

		await this.updateBridgeAmount();
		await this.validateBridgeRequest();
	}

	async initializeMinAndMaxAmountLinks(): Promise<void>
	{
		proposalMinBridgeAmount.addEventListener('click', this.enterMinAmount.bind(this));
		proposalMaxBridgeAmount.addEventListener('click', this.enterMaxAmount.bind(this));
	}

	async updateSmartPoolBalance(): Promise<void>
	{
		// Fetch the current balance from the network.
		const balance256 = await this.smartProvider.getBalance(this.smartLiquidityAddress);

		// Store the balance as BCH equivalent units.
		this.smartLiquidityValue = Number(ethers.utils.formatUnits(balance256));

		// Update the UI to indicate whether there is sufficient liquidity to complete the Pending Request (if there is one).
		await this.updatePendingRequestLiquidityStatus();
	}

	/**
	 * Monitor changes to the amount input and update form button content dynamically.
	 */
	async initializeAmountInput(): Promise<void>
	{
		proposedBridgeAmount.addEventListener('keyup', this.updateBridgeAmount.bind(this), false);
		proposedBridgeAmount.addEventListener('change', this.updateBridgeAmount.bind(this), false);
	}

	/**
	 * Monitor changes to the address input and validate accordingly.
	 */
	async initializeAddressInput(): Promise<void>
	{
		proposedPayoutAddress.addEventListener('keyup', this.validateBridgeRequest.bind(this), false);
		proposedPayoutAddress.addEventListener('change', this.validateBridgeRequest.bind(this), false);
	}

	async initializeCopyToClipboard(): Promise<void>
	{
		paymentAmount.addEventListener('click', this.copyInputToClipboard.bind(this, paymentAmountInput), false);

		paymentCashAddress.addEventListener('click', this.copyInputToClipboard.bind(this, paymentCashAddressInput), false);
		paymentLegacyAddress.addEventListener('click', this.copyInputToClipboard.bind(this, paymentLegacyAddressInput), false);

		requestTransactionToBridge.addEventListener('click', this.copyInputToClipboard.bind(this, requestTransactionToBridgeInput), false);
		requestTransactionFromBridge.addEventListener('click', this.copyInputToClipboard.bind(this, requestTransactionFromBridgeInput), false);
	}

	async updateBridgeAmount(): Promise<void>
	{
		await updateBridgeButtonAmount();

		// Validate the amount for this bridge request.
		await this.validateBridgeRequest();

		// Update the estimated time this bridge request will take.
		await this.estimateBridgeSpeed();
	}

	async validateBridgeAmount(): Promise<void>
	{
		await updateProposalBridgeAmountValidity();
	}

	async validateBridgePayoutAddress(): Promise<void>
	{
		const currentlyProposedPayoutAddress = await getProposedPayoutAddress();

		if(await proposedDirectionIsToCash())
		{
			const cashPayoutAddressIsValid = await isValidCashAddress(currentlyProposedPayoutAddress);

			// Set or remove validation error as necessary.
			if(cashPayoutAddressIsValid)
			{
				// Mark the input as valid.
				await validateProposedBridgePayoutAddress();
			}
			else
			{
				// Mark the input as invalid.
				await invalidateProposedBridgePayoutAddress('INVALID_CASH_OR_LEGACY_ADDRESS');
			}
		}
		else
		{
			const smartPayoutAddressIsValid = await isValidSmartAddress(currentlyProposedPayoutAddress);

			if(smartPayoutAddressIsValid)
			{
				// Mark the input as valid.
				await validateProposedBridgePayoutAddress();
			}
			else
			{
				// Mark the input as invalid.
				await invalidateProposedBridgePayoutAddress('INVALID_SMART_ADDRESS');
			}
		}
	}

	async validateBridgeRequest(): Promise<void>
	{
		// Only update the proposal button if we are not in the process of sending a Smart Transaction.
		// NOTE: We do this as we want to ensure the button remains disabled while waiting for transaction to send (and nothing else enables it).
		if(!this.isWaitingForWalletTransaction)
		{
			// Validate the bridge amount is valid.
			await this.validateBridgeAmount();

			// Validate the payout address is valid.
			await this.validateBridgePayoutAddress();

			// Update the validity of the submit button.
			await updateProposalButtonValidity();
		}
	}

	async initiateBridgeRequest(): Promise<void>
	{
		// ...
		if(await proposedDirectionIsToCash())
		{
			const amount = await getProposedBridgeAmount();
			const payoutAddress = await getProposedPayoutAddress();

			await this.bridgeToCash(payoutAddress, amount);
		}

		if(await proposedDirectionIsToSmart())
		{
			// ...
			const amount = await getProposedBridgeAmount();
			const payoutAddress = await getProposedPayoutAddress();

			// Request a signature if we do not yet have one for this account.
			if(!this.userAccountSignatures.get(this.smartAccountAddress))
			{
				const signature = await this.signStatement(this.smartAccountAddress);

				// If the user did not provide a signature..
				if(!signature)
				{
					// TODO: Inform the user that they need to provide a signature to use this service.

					// Cancel the bridge request initiation.
					return;
				}

				this.userAccountSignatures.set(this.smartAccountAddress, signature);

				// Store the updated signatures in local storage.
				await set('userAccountSignatures', this.userAccountSignatures);
			}

			const accountSignature = this.userAccountSignatures.get(this.smartAccountAddress);

			// If we still don't have an account signature, throw an error.
			if(!accountSignature)
			{
				throw(new Error('Could not initiate bridge request, assumption that account signature already exist did not hold.'));
			}

			// ...
			await this.bridgeToSmart(payoutAddress, amount, accountSignature);
		}
	}

	async initializeBridgeSubmitButton(): Promise<void>
	{
		proposalForm.addEventListener('submit', this.initiateBridgeRequest.bind(this));
	}

	async initializePaymentCancelButton(): Promise<void>
	{
		paymentForm.addEventListener('submit', this.cancelPayment.bind(this));
	}

	async initializeRequestSubmitButton(): Promise<void>
	{
		requestCompleteButton.addEventListener('click', this.closeRequest.bind(this));
	}

	async initializeCloseIntroductionButton(): Promise<void>
	{
		introductionForm.addEventListener('submit', hideIntroduction);
	}

	async initializeDebugState(): Promise<void>
	{
		const debugIsRequested = (window.location.hash === '#debug');
		const debugIsEnabled = (!!await get('debug'));

		// If the current debug setting differs from the requested debug setting..
		if(debugIsRequested !== debugIsEnabled)
		{
			// Update debug logging based on requested setting.
			if(debugIsRequested)
			{
				await set('debug', '*');
			}
			else
			{
				await del('debug');
			}

			// Reload page to apply debug setting.
			window.location.reload();
		}

		if(debugIsEnabled)
		{
			await enableLayoutDebugging();
		}
	}

	//
	async initializeFrontend(): Promise<void>
	{
		// Initialize Debug State.
		await this.initializeDebugState();

		// Initializes translations by:
		// 1. Binding language button click listeners.
		// 2. Getting the last language used from IndexedDB (or defaults to English).
		// 3. And then translate the User Interface into that language.
		await initializeTranslationSupport();

		// Load the account signatures from local storage.
		this.userAccountSignatures = await get('userAccountSignatures') || new Map();

		// Load the current bridge request from local storage.
		this.bridgeRequest = await get('bridgeRequest');

		// Check if the user has been here before.
		const userHasBeenHereBefore = await get('userHasBeenHereBefore');

		// Show the introduction if this is the first time the user visits the page.
		if(!userHasBeenHereBefore)
		{
			// Mark this user as having been here before.
			await set('userHasBeenHereBefore', true);

			// Show the introduction overlay.
			await showIntroduction();
		}

		// Initialize empty proposal, payment and request forms.
		await clearProposalForm(this.smartAccountAddress);
		await clearPaymentForm();
		await clearRequestForm();

		await this.initializeFrontendConstants();
		await this.initializeBitcoinCash();
		await this.initializeAmountInput();
		await this.initializeAddressInput();
		await this.initializeMinAndMaxAmountLinks();
		await this.initializeBridgeDirectionToggle();
		await this.initializeBridgeSubmitButton();
		await this.initializePaymentCancelButton();
		await this.initializeCopyToClipboard();
		await this.initializeRequestSubmitButton();
		await this.initializeCloseIntroductionButton();
		await this.initializeHelpToggle();
		await this.initializePromotionLink();

		await this.estimateBridgeSpeed();

		// HACK: The ImToken wallet does not contain the isMetaMask property, needed by isMetaMaskInstalled().
		//       To allow the ImToken wallet to be used, we instead check this as a separate condition.
		const isImTokenWallet = ((window as any).ethereum && (window as any).ethereum.isImToken);

		// Check whether Metamask (or ImToken wallet) is installed/available.
		if(!MetaMaskOnboarding.isMetaMaskInstalled() && !isImTokenWallet)
		{
			await this.showInstallMetamaskDialog();

			return;
		}

		await this.completeDialog('installWallet');

		// Wait for Metamask to be initialized.
		// NOTE: This is used as a workaround to a Metamask bug.
		//       See function implementation for more details.
		await this.waitForMetamaskToBeInitialized();

		// NOTE: the ethereum provider is injected by a plugin into the global window object at runtime, and
		//       as such, typescript can only enforce typing on it if I manually provide a set a types for it.
		const ethereumProvider = (window as any).ethereum as any;

		// Setup event handlers for all metamask provided events.
		// TODO: Handle connect/disconnect from blockchain events properly.
		// NOTE: The connect/disconnect is when metamask acquires or loses connection to the SmartBCH RPC server.
		ethereumProvider.on('connect', console.log);
		ethereumProvider.on('disconnect', console.log);
		ethereumProvider.on('accountsChanged', this.handleAccountChanges.bind(this));
		ethereumProvider.on('chainChanged', this.handleChainChanges.bind(this));

		// ...
		this.smartProvider = new ethers.providers.Web3Provider(ethereumProvider, SMART_CHAIN_ID);

		// Create an interface to the smart contract, connected to the network via the provider.
		this.smartContract = new ethers.Contract(SMART_CONTRACT_ADDRESS, SMART_CONTRACT_ABI, this.smartProvider);

		// Store the metamask signer as our internal signer for SmartBCH related interactions.
		// @ts-ignore
		this.smartSigner = this.smartProvider.getSigner();

		// Wait for Metamask to be unlocked (and show Unlock Metamask dialog if necessary).
		await this.waitForMetamaskToBeUnlocked();

		// Wait for Metamask to be using the SmartBCH network (and show Setup Network dialog if necessary).
		await this.waitForMetamaskSmartNetworkToBeActive();

		// Wait for the user to have a wallet connected (and show Connect Wallet dialog if necessary).
		const accountAddress = await this.waitForMetamaskAccountToBeConnected();

		// Trigger chain change code with the requested chain id
		await this.handleChainChanges(SMART_CHAIN_ID_HEX);

		// Trigger account change code with the Metamask Wallet address.
		await this.handleAccountChanges([ accountAddress ]);

		// Update the Smart Account Balance whenever a new Smart Block is mined.
		// NOTE: As of 2022-03-29, the Ethers library provides no way of monitoring an EOA account balance.
		//       Instead, we have to fetch the balance upon each new Smart Block.
		//       See: https://github.com/ethers-io/ethers.js/issues/135
		this.smartProvider.on('block', this.handleNewSmartBlock.bind(this));

		// If there is a Bridge Request pending, but it has not been forwarded to bridge yet...
		if(this.bridgeRequest && !this.bridgeRequest.sourceTransactionHash)
		{
			// Fetch the appropriate account signature based on the current requests payout address.
			const accountSignature = this.userAccountSignatures.get(this.bridgeRequest.payoutAddress);

			// If no account signature exist for this payout address..
			if(!accountSignature)
			{
				throw(new Error('Could not handlePendingRequestFunds, assumption that account signature already exist did not hold.'));
			}

			// Derive Hop Wallet address from the current account signature.
			const { cashAddress } = await this.deriveWalletFromSignature(accountSignature);

			// Fetch all unspent transaction outputs for the temporary in-browser wallet.
			const unspentOutputs = await getUnspentOutputs(this.cashProvider, cashAddress);

			// Calculate the total balance of the unspent outputs.
			const accountBalance = unspentOutputs.reduce((totalValue, unspentOutput) => (totalValue + unspentOutput.value), 0);

			// If the account balance is zero, user submitted a proposal when they last visited, but did not deposit any funds...
			if(accountBalance === 0)
			{
				// Let's just show the user the Proposal Screen and have them start again.
				await showProposalScreen();
			}
			// Otherwise, there are funds and the user should continue their bridge request...
			else
			{
				// Subscribe to payments on the user's Hop Wallet.
				// NOTE: We do this here because the Hop Wallet balance may be less than Bridge Minimum.
				//       When a user sends more, we want to re-trigger handlePendingRequestFunds to action the forward to bridge.
				await this.cashProvider.subscribe(this.handlePendingRequestFunds.bind(this), 'blockchain.address.subscribe', cashAddress);

				// Handle the pending request funds (forward if above minimum, wait for more if below).
				await this.handlePendingRequestFunds();
			}
		}
		// Otherwise, if there is a Bridge Request pending and it has been forwarded to the bridge...
		else if(this.bridgeRequest && this.bridgeRequest.sourceTransactionHash)
		{
			// Prevent users from leaving the request screen until the bridging has completed.
			// TODO: Uncomment below once we identify conditions where completed Bridge Transactions are not recognized by frontend.
			// await disableRequestButton();

			// Update the user interface to show a pending icon.
			await updateRequestStatus('pending', true);

			// Show the amount forwarded to the user.
			await setRequestAmountReceived(this.bridgeRequest.bridgeAmount);

			// Show the current requests bridge transaction hash.
			await setRequestBridgeTransaction(this.bridgeRequest.sourceTransactionHash);

			// Check if the request is actually already paid out..
			if(this.bridgeRequest.payoutTransactionHash)
			{
				// Show the current request payout transaction hash.
				await setRequestPayoutTransaction(this.bridgeRequest.payoutTransactionHash);

				// Update the user interface to show a completed icon.
				await updateRequestStatus('complete', false);

				// Allow the user to leaving the request screen as the bridging has been completed.
				await enableRequestButton();
			}
			// Otherwise, if the incomplete Bridge Request is from Cash To Smart...
			else if(this.bridgeRequest.bridgeDirection === CASH_TO_SMART)
			{
				// Update and show the number of confirmations for this bridge transaction.
				await this.updateCashConfirmations();

				// TODO: Deduplicate this code.
				// Set up a filter that looks at the bridge payout contract for transactions claiming to be paying out for the current request.
				const filterForBridgePayoutEvents = this.smartContract.filters.Bridged(hexToBin(this.bridgeRequest.sourceTransactionHash));

				// Look for evidence that the payout has already happened..
				const filterLogs = await this.smartProvider.getLogs({ ...filterForBridgePayoutEvents, fromBlock: 0, toBlock: 'latest' });

				// Update user interface accordingly if we have a payout event.
				if(filterLogs.length > 0)
				{
					// Show the current request payout transaction hash.
					await setRequestPayoutTransaction(filterLogs[0].transactionHash);

					// Update the user interface to show a completed icon.
					await updateRequestStatus('complete', false);

					// Allow the user to leaving the request screen as the bridging has been completed.
					await enableRequestButton();
				}
				else
				{
					// Set up an event handler for this payout events matching this filter.
					this.smartContract.on(filterForBridgePayoutEvents, this.handleSmartBridgePayouts.bind(this));
				}
			}
			// Otherwise, the incomplete Bridge Request is from Smart to Cash.
			else
			{
				// Assume the transaction has not confirmed yet.
				await setBridgeRequirement('CONFIRMATIONS', '0/1');

				// NOTE: provider.getTransactionReceipt(hash) will result in "null" if not mined
				const bridgeTransactionReceipt = await this.smartProvider.getTransactionReceipt(this.bridgeRequest.sourceTransactionHash);

				// If the transaction is known and has been confirmed, update the user interface.
				if(bridgeTransactionReceipt && bridgeTransactionReceipt.blockNumber)
				{
					await setBridgeRequirement('CONFIRMATIONS', '1/1');
				}
				else
				{
					// Update the confirmation when it gets included in a block.
					// NOTE: This is run async to be non-blocking.
					this.updateSmartConfirmationOnBlockInclusion(this.bridgeRequest.sourceTransactionHash);
				}
			}

			// Show the request follow-up screen.
			await showRequestScreen();
		}
		// Otherwise, there is no Bridge Request and we should just show the Proposal Screen.
		else
		{
			// Show the proposal view.
			await showProposalScreen();
		}
	}

	async evaluateRequirements(): Promise<void>
	{
		const installPending = installWallet.classList.contains('pending');
		const walletPending = connectWallet.classList.contains('pending');
		const unlockPending = unlockWallet.classList.contains('pending');
		const networkPending = setupNetwork.classList.contains('pending');

		if(installPending || unlockPending || networkPending || walletPending)
		{
			await showRequirementsOverlay();
		}
		else
		{
			await hideRequirementsOverlay();
		}
	}

	async checkIfMetaMaskIsUnlocked(): Promise<void>
	{
		try
		{
			// NOTE: the ethereum provider is injected by a plugin into the global window object at runtime, and
			//       as such, typescript can only enforce typing on it if I manually provide a set a types for it.
			const ethereumProvider = (window as any).ethereum as any;

			// If the experimental isUnlocked function is present on the current metamask install..
			/* eslint-disable-next-line no-underscore-dangle */
			if(typeof ethereumProvider._metamask.isUnlocked !== 'undefined')
			{
				// .. we can ask the user to unlock their wallet if it is known to be locked.
				/* eslint-disable-next-line no-underscore-dangle */
				if(!await ethereumProvider._metamask.isUnlocked())
				{
					await this.showUnlockDialog();
				}
				else
				{
					await this.completeDialog('unlockWallet');
				}
			}
		}
		catch(error)
		{
			// Do nothing, as this feature was experimental and could not be trusted anyway.
		}
	}

	/**
	 * Waits for Metamask to be initialized and reloads the page if it is not initialized.
	 *
	 * @note This is a workaround to a bug in Metamask's Browser Extension. See https://gitlab.com/GeneralProtocols/smartbch/hop-cash/frontend/-/issues/43
	 */
	async waitForMetamaskToBeInitialized(): Promise<void>
	{
		// NOTE: This function is a specific workaround to a bug in Metamask's Browser Extension whereby, upon launching the browser, there can be a delay
		//       before the Metamask Browser Extension is actually ready for use. If we load a page before Metamask is ready, the Metamask Browser Extension
		//       enters an irrecoverable broken state with the only solution being to reload the page.
		//       As a workaround to this we:
		//       Check if this Smart Wallet identifies as a Metamask Wallet using window.ethereum.isMetaMask() (and just return to exit function if not).
		//       Check if this Smart Wallet has the window.ethereum._state.initialized property and, if it is false, reload the page until it is ready.
		//       For wallets that identify as Metamask but do not have this 'initialized' property, we simply skip reloading as this workaround is not necessary.

		try
		{
			// NOTE: The ethereum provider is injected by a plugin into the global window object at runtime, and
			//       as such, typescript can only enforce typing on it if I manually provide a set a types for it.
			const ethereumProvider = (window as any).ethereum as any;

			// If window.ethereum is a falsey value, we have attempted to call this function on a browser that does not have a Smart Wallet installed.
			if(!ethereumProvider)
			{
				throw(new Error('Metamask (window.ethereum) unavailable. Did you forget to check if Metamask was installed before calling this function?'));
			}

			// If the Smart Wallet does not claim to be Metamask, return as we do not require this workaround.
			if(!ethereumProvider.isMetaMask)
			{
				return;
			}

			// Evaluate expressions to determine whether:
			// a) This Smart Provider actually has a _state.initialized property (compatible but non-Metamask Wallets may not have this).
			// b) This initialized property (if it exists) is set to true.
			/* eslint-disable-next-line no-underscore-dangle */
			const hasInitializedProperty = (ethereumProvider._state && Object.prototype.hasOwnProperty.call(ethereumProvider._state, 'initialized'));

			// If the _state.initialized property does not exist, then this workaround is not necessary.
			if(!hasInitializedProperty)
			{
				return;
			}

			// Evaluate the state of the initialized property.
			/* eslint-disable-next-line no-underscore-dangle */
			const isInitialized = (ethereumProvider._state.initialized);

			// If the Smart Wallet has the initialized property and it is false (i.e. Metamask is uninitialized)...
			if(!isInitialized)
			{
				// Add a small delay to prevent unnecessarily hammering the user with page reloads while we wait for Metamask to initialize.
				await this.delay(500);

				// Reload the page as this Metamask bug requires a refresh of the page.
				window.location.reload();
			}
		}
		catch(error)
		{
			// This is a hack workaround to a Metamask bug and could not be trusted anyway, so let's just log a warning.
			// More information: https://gitlab.com/GeneralProtocols/smartbch/hop-cash/frontend/-/issues/43
			console.warn(`waitForMetamaskToBeInitialized(): ${error}`);
		}
	}

	/**
	 * Waits for Metamask to be unlocked and will show the Unlock Metamask Dialog until it is unlocked.
	 */
	async waitForMetamaskToBeUnlocked(): Promise<void>
	{
		// TODO: Consider using this function elsewhere where we need to check for this condition.

		try
		{
			// NOTE: The ethereum provider is injected by a plugin into the global window object at runtime, and
			//       as such, typescript can only enforce typing on it if I manually provide a set a types for it.
			const ethereumProvider = (window as any).ethereum as any;

			// If window.ethereum is a falsey value, we have attempted to call this function on a browser that does not have a Smart Wallet installed.
			if(!ethereumProvider)
			{
				throw(new Error('Metamask (window.ethereum) unavailable. Did you forget to check if Metamask was installed before calling this function?'));
			}

			// If the Smart Wallet does not claim to be Metamask, return as it will likely not have this feature.
			if(!ethereumProvider.isMetaMask)
			{
				return;
			}

			// If the _metamask property does not exist on our window.ethereum provider, then the unlocked method will not be supported so just return.
			/* eslint-disable-next-line no-underscore-dangle */
			if(typeof ethereumProvider._metamask === 'undefined')
			{
				return;
			}

			// If the experimental isUnlocked function is present on the current metamask install..
			/* eslint-disable-next-line no-underscore-dangle */
			if(typeof ethereumProvider._metamask.isUnlocked !== 'undefined')
			{
				// .. we can ask the user to unlock their wallet if it is known to be locked.
				/* eslint-disable-next-line no-underscore-dangle */
				while(!await ethereumProvider._metamask.isUnlocked())
				{
					// As Metamask is locked, show the Unlock Dialog.
					await this.showUnlockDialog();

					// Delay the loop for 200 milliseconds to prevent hammering the Metamask API.
					await this.delay(200);
				}

				// Hide the unlock wallet dialog.
				await this.completeDialog('unlockWallet');
			}
			// Otherwise, output a debug message indicating that this feature is unavailable.
			else
			{
				// NOTE: There is not much we can do to handle this case at this point in time (2022-04-20).
				//       If this check is unavailable, we rely on waitForMetamaskSmartNetworkToBeActive() and waitForMetamaskAccountToBeConnected()
				//       to halt the flow until Metamask is in a workable state with Hop.Cash.
				//       The consequence of this is that the user will see a "Setup Network" or "Connect Wallet" dialog as opposed to the "Unlock Wallet" dialog.
				console.warn('waitForMetamaskToBeUnlocked(): Experimental Metamask isUnlocked() feature unavailable with current Smart Wallet.');
			}
		}
		catch(error)
		{
			// This feature was experimental and could not be trusted anyway, so let's just log a warning.
			console.warn(`waitForMetamaskToBeUnlocked(): ${error}`);
		}
	}

	/**
	 * Waits for the user to set the SmartBCH Chain as active and will show the Setup Network Dialog until it is active.
	 *
	 * @returns {Promise<ethers.providers.Network>} The SmartBCH Network that Metamask is connected to.
	 */
	async waitForMetamaskSmartNetworkToBeActive(): Promise<ethers.providers.Network>
	{
		// TODO: Consider using this function elsewhere where we need to check for this condition.

		// Declare Smart Provider Network here so that is available in broader scope..
		let smartProviderNetwork;

		// Continuously loop until user has added and set the SmartBCH Network as active in Metamask.
		do
		{
			try
			{
				// Attempt to get the the active Network from Metamask..
				// NOTE: This will throw an error if the network returned has a different chain than the one we initially configured with our Ethers Provider (SmartBCH).
				smartProviderNetwork = await this.smartProvider.getNetwork();

				// As an additional safe-guard, make sure the Chain ID equals our SmartBCH Chain ID.
				// NOTE: Given the above, this should always be the case but we do this to guard against any changes/bugs in Ethers behavior.
				if(smartProviderNetwork.chainId !== SMART_CHAIN_ID)
				{
					throw(new Error(`Smart Provider Chain ID (${smartProviderNetwork.chainId}) does not equal Smart BCH Chain ID (${SMART_CHAIN_ID}).`));
				}
			}
			catch(error: any)
			{
				// We expect that the user may change the Metamask network but any other reason is unexpected and we should log it for debugging purposes.
				if(error.reason !== 'underlying network changed')
				{
					console.warn('Failed to get network information:');
					console.warn(JSON.stringify(error));
				}

				// As the SmartBCH Network is not active, show the Setup Network dialog.
				await this.showSetupNetworkDialog();

				// Delay the loop for 200 milliseconds to prevent hammering the Metamask API.
				await this.delay(200);
			}
		}
		while(!smartProviderNetwork);

		// Metamask is connected to the SmartBCH network so make sure that the Setup Network Dialog is hidden.
		await this.completeDialog('setupNetwork');

		// Return the Smart Provider Network.
		return smartProviderNetwork;
	}

	/**
	 * Waits for the user to connect a Metamask Account and will show the Connect Account Dialog until one is connected.
	 *
	 * @returns {Promise<string>} The address of the connected Metamask Account.
	 */
	async waitForMetamaskAccountToBeConnected(): Promise<string>
	{
		// TODO: Consider using this function elsewhere where we need to check for this condition.

		// Declare Metamask Account Address here so that it is available in the broader scope.
		let accountAddress: string | undefined;

		// Continuously loop until user has connected a Metamask Account to Hop.Cash.
		do
		{
			try
			{
				// Attempt to get the user's Smart Address (indicating user has connected their Smart Wallet to Hop.Cash).
				// NOTE: This will throw an error if Smart Address cannot be retrieved (i.e. User has not yet connected).
				accountAddress = await this.smartSigner.getAddress();
			}
			catch(error)
			{
				// As there is no Smart Account connected, show the Connect Wallet dialog.
				await this.showConnectWalletDialog();

				// Delay the loop for 200 milliseconds to prevent hammering the Metamask API.
				await this.delay(200);
			}
		}
		while(!accountAddress);

		// A Metamask Account is connected so make sure that the Connect Wallet Dialog is hidden.
		await this.completeDialog('connectWallet');

		// Return the Metamask Account Address.
		return accountAddress;
	}

	async handleChainChanges(chainId: string): Promise<void>
	{
		if(chainId !== SMART_CHAIN_ID_HEX)
		{
			await this.showSetupNetworkDialog();
		}
		else
		{
			await this.completeDialog('setupNetwork');

			// Update the balance in the user's Smart Account.
			await this.updateSmartAccountBalance();

			// Update the Smart Liquidity Pool balance.
			await this.updateSmartPoolBalance();
		}
	}

	/**
	 * Updates the User's Smart Balance and Liquidity Pool balance.
	 */
	async handleNewSmartBlock(): Promise<void>
	{
		// Update the balance in the user's Smart Account.
		await this.updateSmartAccountBalance();

		// Update the Smart Liquidity Pool balance.
		await this.updateSmartPoolBalance();

		// Update the maximum amount.
		await this.updateMaxAmount();
	}

	async showUnlockDialog(): Promise<void>
	{
		await this.activateDialog('unlockWallet', this.checkIfMetaMaskIsUnlocked.bind(this));
	}

	async showSetupNetworkDialog(): Promise<void>
	{
		// NOTE: the ethereum provider is injected by a plugin into the global window object at runtime, and
		//       as such, typescript can only enforce typing on it if I manually provide a set a types for it.
		const ethereumProvider = (window as any).ethereum as any;

		const addNetworkRequest =
		{
			method: 'wallet_addEthereumChain',
			params: [ SMART_CHAIN_PARAMETERS ],
		};

		await this.activateDialog('setupNetwork', ethereumProvider.request.bind(this, addNetworkRequest));
	}

	async handleAccountChanges(accounts: Array<string>): Promise<void>
	{
		// Check if metamask is unlocked, as locking emits an account changed event.
		await this.checkIfMetaMaskIsUnlocked();

		if(accounts.length === 0)
		{
			await this.showConnectWalletDialog();
		}
		else
		{
			await this.completeDialog('connectWallet');

			// Get the address of the selected MetaMask account.
			// NOTE: We do not want to use the accounts payload given in the callback.
			//       This gives a lower-case (non-checksum) address which breaks our key derivation.
			this.smartAccountAddress = await this.smartSigner.getAddress();

			// Update the Smart Account Balance.
			await this.updateSmartAccountBalance();

			// Update the user interface to show the new account address (if our direction is Cash to Smart).
			if(await proposedDirectionIsToSmart())
			{
				await setProposedPayoutAddress(this.smartAccountAddress);
			}
		}
	}

	/**
	 * Updates the balance of the user's active Smart Account Address.
	 */
	async updateSmartAccountBalance(): Promise<void>
	{
		// Make sure an address is set to prevent unusual edge-cases or incorrect calling.
		if(!this.smartAccountAddress)
		{
			return;
		}

		// Fetch the current balance from the network.
		const balance256 = await this.smartProvider.getBalance(this.smartAccountAddress);

		// Store the balance as BCH equivalent units.
		this.smartAccountBalance = Number(ethers.utils.formatUnits(balance256));
	}

	async signStatement(accountAddress: string): Promise<null | string>
	{
		try
		{
			//
			const message = `[Grant-Hop-Wallet]【授权Hop钱包】\n${accountAddress}\nI hereby grant this website the permission to access my Hop-Wallet for above address.\n我郑重授权此网站访问以上地址的Hop钱包。`;
			const signature = await this.smartSigner.signMessage(message);

			return signature;
		}
		catch(error: any)
		{
			// If the error is the user cancelling the signature request..
			if(error['code'] === 4001)
			{
				// Do nothing
				return null;
			}

			// Do not log and instead throw a new error, effectively hiding any potentially sensitive signature information from the console.
			throw(new Error('Could not sign statement.'));
		}
	}

	async showConnectWalletDialog(): Promise<void>
	{
		// NOTE: the ethereum provider is injected by a plugin into the global window object at runtime, and
		//       as such, typescript can only enforce typing on it if I manually provide a set a types for it.
		const ethereumProvider = (window as any).ethereum as any;

		const connectWalletRequest =
		{
			method: 'eth_requestAccounts',
		};

		await this.activateDialog('connectWallet', ethereumProvider.request.bind(this, connectWalletRequest));
	}

	async activateDialog(elementId: string, onclick: any): Promise<void>
	{
		// Fetch a reference to the dialog.
		const dialogElement = document.getElementById(elementId)!;

		// Attach the onboarder to the dialog, then show the dialog.
		dialogElement.onclick = onclick;
		dialogElement.className = 'pending';

		//
		await this.evaluateRequirements();
	}

	async completeDialog(elementId: string): Promise<void>
	{
		// Fetch a reference to the dialog.
		const dialogElement = document.getElementById(elementId)!;

		// Hide the dialog as completed.
		dialogElement.className = 'complete';

		//
		await this.evaluateRequirements();
	}

	async showInstallMetamaskDialog(): Promise<void>
	{
		// Create an instance of the metamask onboarding utility.
		const metaMaskOnboarder = new MetaMaskOnboarding();

		await this.activateDialog('installWallet', metaMaskOnboarder.startOnboarding);
	}

	async closeRequest(): Promise<void>
	{
		// Reset the proposal and payment form before showing the bridge interface again.
		await clearProposalForm(this.smartAccountAddress);
		await clearPaymentForm();

		// Update the max amount you can send.
		await this.updateMaxAmount();

		// Show the bridge view.
		await showProposalScreen();

		// Reset the request form now that we're showing the bridge form.
		await clearRequestForm();

		// Set the internal presentation for the request to null.
		await this.markRequestAsNull();
	}

	async cancelPayment(): Promise<void>
	{
		// Reset the proposal and request form before showing the bridge interface again.
		await clearProposalForm(this.smartAccountAddress);
		await clearRequestForm();

		// Update the max amount you can send.
		await this.updateMaxAmount();

		// Go back to the bridge view.
		await showProposalScreen();

		// Reset payment details.
		await clearPaymentForm();
	}

	async markRequestAsNull(): Promise<void>
	{
		delete this.bridgeRequest;

		// Store the updated bridge request in local storage.
		await set('bridgeRequest', this.bridgeRequest);
	}

	async markRequestAsNew(bridgeDirection: number, bridgeAmount: number, payoutAddress: string): Promise<void>
	{
		// Store this bridge request as the currently pending request.
		this.bridgeRequest = { bridgeDirection, bridgeAmount, payoutAddress };

		// Store the updated bridge request in local storage.
		await set('bridgeRequest', this.bridgeRequest);
	}

	/**
	 * Marks the currently new request as pending, and updates the user interface to inform the user.
	 *
	 * @param sourceTransactionHash   identifier for the transaction that created the request.
	 */
	async markRequestAsPending(sourceTransactionHash: string): Promise<void>
	{
		if(!this.bridgeRequest)
		{
			throw(new Error(`Could not mark non-existent request (${sourceTransactionHash}) as pending.`));
		}

		// Throw an error if we're trying to mark a *different* request as pending.
		if((this.bridgeRequest.sourceTransactionHash) && (this.bridgeRequest.sourceTransactionHash !== sourceTransactionHash))
		{
			throw(new Error(`Could not mark an existing request (${this.bridgeRequest.sourceTransactionHash}) as pending, as it already has a sourceTransaction different from the one provided (${sourceTransactionHash}).`));
		}

		// Store the source transaction for this request.
		this.bridgeRequest.sourceTransactionHash = sourceTransactionHash;

		// Store the updated bridge request in local storage.
		await set('bridgeRequest', this.bridgeRequest);

		// Update the user interface to show a pending icon.
		await updateRequestStatus('pending', true);
	}

	/**
	 * Marks the currently pending request as completed, and updates the user interface to inform the user.
	 *
	 * @param sourceTransactionHash   identifier for the transaction that created the request.
	 * @param payoutTransactionHash   identifier for the transaction that fulfilled the payout for the request.
	 */
	async markRequestAsComplete(sourceTransactionHash: string, payoutTransactionHash: string): Promise<void>
	{
		if(!this.bridgeRequest)
		{
			throw(new Error(`Could not mark non-existent request (${sourceTransactionHash}) as completed.`));
		}

		// Throw an error if we're trying to mark a *different* request as completed.
		if(this.bridgeRequest.sourceTransactionHash && (this.bridgeRequest.sourceTransactionHash !== sourceTransactionHash))
		{
			throw(new Error(`Could not mark an existing request (${this.bridgeRequest.sourceTransactionHash}) as complete, as it is using a different sourceTransaction from the one provided (${sourceTransactionHash}).`));
		}

		// Throw an error if we're trying to overwrite the requests already existing payout transaction hash.
		if(this.bridgeRequest.payoutTransactionHash && (this.bridgeRequest.payoutTransactionHash !== payoutTransactionHash))
		{
			throw(new Error(`Could not mark an existing request (${this.bridgeRequest.sourceTransactionHash}) as complete, as it already has a payoutTransactionHash different from the one provided (${payoutTransactionHash}).`));
		}

		// Store the source transaction for this request.
		this.bridgeRequest.payoutTransactionHash = payoutTransactionHash;

		// Store the updated bridge request in local storage.
		await set('bridgeRequest', this.bridgeRequest);

		// Update the user interface to show a completed icon.
		await updateRequestStatus('complete', false);
	}

	async updateUserAddressHistory(): Promise<void>
	{
		// Exit early if there is nothing to monitor for.
		if(!this.bridgeRequest || !this.bridgeRequest.payoutAddress)
		{
			// Do nothing.
			return;
		}

		// Exit early if the payout address is invalid for the cash side of the bridge.
		const cashPayoutAddressIsValid = await isValidCashAddress(this.bridgeRequest.payoutAddress);
		if(!cashPayoutAddressIsValid)
		{
			// Do nothing.
			return;
		}

		// Fetch a list of transactions relating to the payout address for the provided request.
		// TODO: Set up the type definitions for get_history.
		// @ts-ignore
		const payoutHistory = await this.cashProvider.request('blockchain.address.get_history', this.bridgeRequest.payoutAddress) as Array<any>;

		const transactionParsingPromises = [];

		for(const transactionData of payoutHistory)
		{
			const transactionHash = transactionData['tx_hash'];
			const transactionParsingPromise = this.storeCashBridgePayout(transactionHash);
			transactionParsingPromises.push(transactionParsingPromise);
		}

		// Wait for all transactions to be parsed.
		await Promise.all(transactionParsingPromises);

		// Check and display if a payout for the current request is known.
		await this.detectBridgePayout();
	}

	async storeSmartBridgePayout(requestTransactionHash: string, transactionHash: string): Promise<void>
	{
		// Store this transaction as fulfilling the provided request.
		this.userBridgeRequests.set(requestTransactionHash, transactionHash);
	}

	async storeCashBridgePayout(transactionHash: string): Promise<void>
	{
		// Skip this transaction if it has already been parsed before.
		// TODO: Either store this in local storage, or find some other way to avoid full scan before initiating request.
		if(this.userParsedTransactions.includes(transactionHash))
		{
			return;
		}

		// Fetch the full transaction from the network.
		const transactionHex = await getTransaction(this.cashProvider, transactionHash);

		// Declare our sourceTransaction so that it is in scope for rest of function.
		let sourceTransaction: string;

		// Attempt to parse the transaction to determine if it's a bridge payout transaction.
		try
		{
			({ sourceTransaction } = await parseCashPayoutTransaction(transactionHex));
		}
		catch(error)
		{
			// If the transaction is not a valid payout transaction, return.
			return;
		}

		// Check if the transaction is a bridge payout transaction.
		if(sourceTransaction)
		{
			// Store this transaction as fulfilling the provided request.
			this.userBridgeRequests.set(sourceTransaction, transactionHash);
		}

		// Mark this transaction as parsed by this user.
		this.userParsedTransactions.push(transactionHash);
	}

	async detectBridgePayout(): Promise<void>
	{
		// Ignore any calls to detect bridge payout if we do not have a pending request to monitor.
		if(!this.bridgeRequest || !this.bridgeRequest.sourceTransactionHash)
		{
			return;
		}

		// Update the displayed payout transaction when known..
		if(this.userBridgeRequests.has(this.bridgeRequest.sourceTransactionHash))
		{
			// Extract the payout transaction hash for the current request..
			const payoutTransactionHash = this.userBridgeRequests.get(this.bridgeRequest.sourceTransactionHash)!;

			// Update the presentation to show the payout transaction hash.
			await setRequestPayoutTransaction(payoutTransactionHash);

			// Mark the request as complete.
			await this.markRequestAsComplete(this.bridgeRequest.sourceTransactionHash, payoutTransactionHash);

			// Enable the request button to allow users to go back to the proposal screen.
			await enableRequestButton();
		}
		// or reset to an empty field otherwise.
		else
		{
			await setRequestPayoutTransaction('');
		}
	}

	/**
	 * If there's a Pending Bridge Request and it exceeds liquidity balance, update UI to show error.
	 * Otherwise, update/keep the UI to show the request as pending.
	 */
	async updatePendingRequestLiquidityStatus(): Promise<void>
	{
		// Return if no active bridge request exists.
		if(!this.bridgeRequest)
		{
			return;
		}

		// Return if the Bridge Request is not in a pending state (i.e. it already has a Payout Transaction Hash).
		if(this.bridgeRequest.payoutTransactionHash)
		{
			return;
		}

		// Determine which Liquidity Pool to check based on our current Bridge Request's direction.
		const liquidityAmount = (this.bridgeRequest.bridgeDirection === CASH_TO_SMART ? this.smartLiquidityValue : this.cashLiquidityValue);

		// If pending request amount exceeds liquidity, set request as having insufficient liquidity and show notice.
		if(this.bridgeRequest.bridgeAmount > liquidityAmount)
		{
			// Update UI to show that there is insufficient liquidity to complete request.
			updateRequestStatus('insufficientLiquidity', true);
		}

		// Otherwise, update/keep request status to pending and hide insufficient liquidity notice.
		else
		{
			// Update UI to show that request is pending.
			updateRequestStatus('pending', true);
		}
	}

	async handleSmartBridgePayouts(sourceTransaction: string, liquidityProviderAddress: string, outputAddress: string, outputAmount: any, eventData: any): Promise<void>
	{
		// ...
		const payoutTransactionHash = eventData.transactionHash;

		// Store this payout as matching the source transactions request.
		await this.storeSmartBridgePayout(sourceTransaction.slice(2), payoutTransactionHash);

		//
		await this.detectBridgePayout();
	}

	async bridgeToSmart(payoutAddress: string, amount: number, accountSignature: string): Promise<void>
	{
		// Derive wallet from the current account signature.
		const { cashAddress, legacyAddress } = await this.deriveWalletFromSignature(accountSignature);

		// Monitor current browser-wallet address for funds to be used to handle the current request.
		// TODO: Figure out how to do this with regards to re-use / multiple bridges in the same session.
		await this.cashProvider.subscribe(this.handlePendingRequestFunds.bind(this), 'blockchain.address.subscribe', cashAddress);

		// Update the payment view to show the payment required to complete this bridge request.
		await updatePaymentDetails(amount, cashAddress, legacyAddress);

		// Internally mark this request as the currently pending bridge request.
		await this.markRequestAsNew(CASH_TO_SMART, amount, payoutAddress);

		// Show the request follow-up interface.
		await showPaymentScreen();

		// Hide the Payment Below Minimum Amount Notice (in case it is visible).
		await hidePaymentBelowMinimumAmountNotice();
	}

	async handlePendingRequestFunds(): Promise<void>
	{
		// Exit early if no current request exist.
		// TODO: Simplify this.
		if(!this.bridgeRequest || !this.bridgeRequest.bridgeAmount || !this.bridgeRequest.payoutAddress)
		{
			return;
		}

		// Fetch the appropriate account signature based on the current requests payout address.
		const accountSignature = this.userAccountSignatures.get(this.bridgeRequest.payoutAddress);

		// If no account signature exist for this payout address..
		if(!accountSignature)
		{
			throw(new Error('Could not handlePendingRequestFunds, assumption that account signature already exist did not hold.'));
		}

		// Derive wallet from the current account signature.
		const { privateKeyWIF, cashAddress, legacyAddress } = await this.deriveWalletFromSignature(accountSignature);

		// Fetch all unspent transaction outputs for the temporary in-browser wallet.
		const unspentOutputs = await getUnspentOutputs(this.cashProvider, cashAddress);

		// Calculate the total balance of the unspent outputs.
		const accountBalance = unspentOutputs.reduce((totalValue, unspentOutput) => (totalValue + unspentOutput.value), 0);

		// Abort if there are no funds available.
		if(accountBalance === 0)
		{
			return;
		}

		// Update the UI to show the amount that the Frontend Wallet has received.
		await updatePaymentAmountReceived(accountBalance / SATOSHIS_PER_BITCOIN_CASH);

		// If there's funds, but not enough to meet bridge minimum - inform the user that they must send more and update the QR codes to match new amount.
		if(accountBalance < MINIMUM_FRONTEND_SATOSHIS)
		{
			// Calculate the amount required to reach the bridge's minimum.
			const requiredToMeetMinimum = MINIMUM_FRONTEND_SATOSHIS - accountBalance;

			// Make sure the amount that we request is above the Satoshi Dust Limit.
			// NOTE: This has the pleasant side-effect of mitigating some Javascript Number formatting quirks by keeping precision to <= 5 decimal places.
			//       Otherwise, this could display to the user in Javascript's Exponent Notation.
			if(requiredToMeetMinimum < CASH_SATOSHI_DUST_LIMIT)
			{
				// Set the amount that we should request to the satoshi dust limit (in Cash/BCH Units).
				const amountToRequest = CASH_SATOSHI_DUST_LIMIT / SATOSHIS_PER_BITCOIN_CASH;

				// Update the payment details to request the new amount.
				await updatePaymentDetails(amountToRequest, cashAddress, legacyAddress);
			}
			else
			{
				// Set the amount that we should request to the amount required to meet minimum (in Cash/BCH Units).
				const amountToRequest = requiredToMeetMinimum / SATOSHIS_PER_BITCOIN_CASH;

				// Update the payment details to request the new amount.
				await updatePaymentDetails(amountToRequest, cashAddress, legacyAddress);
			}

			// Show notice in the UI to indicate underpayment.
			await showPaymentBelowMinimumAmountNotice();

			// Ensure that the payment screen is showing.
			await showPaymentScreen();

			// Disable the Cancel Request Button as the Hop (frontend) Wallet now has funds in it.
			await disablePaymentButton('CANNOT_CANCEL_PARTIALLY_PAID_BRIDGE_REQUEST');

			return;
		}

		// TODO: If there's funds, but it's more than request - ask user if we should bridge everything
		if(accountBalance > this.bridgeRequest.bridgeAmount * SATOSHIS_PER_BITCOIN_CASH)
		{
			console.log('Wallet holds more than expected amount, bridging everything.');
			// return;
		}

		// Disable the cancel button while we build the bridge transaction.
		await disablePaymentButton('BRIDGING_IS_NOW_IN_PROGRESS');

		// Create a bridge transaction without miner fee to determine the transaction size and therefor the miner fee.
		const transactionTemplate = await createCashBridgeTransaction(privateKeyWIF, unspentOutputs, this.cashReceivingAddress, this.bridgeRequest.payoutAddress, 0);
		const minerFee = 1 * transactionTemplate.byteLength;

		// If there's funds and it matches our expectation, forward it to the bridge.
		const bridgeTransaction = await createCashBridgeTransaction(privateKeyWIF, unspentOutputs, this.cashReceivingAddress, this.bridgeRequest.payoutAddress, minerFee);

		// Broadcast the transaction.
		const bridgeTransactionHash = await broadcastTransaction(this.cashProvider, binToHex(bridgeTransaction), false);

		// Set up a filter that looks at the bridge payout contract for transactions claiming to be paying out for the current request.
		const filterForBridgePayoutEvents = this.smartContract.filters.Bridged(hexToBin(bridgeTransactionHash));

		// Set up an event handler for this payout events matching this filter.
		this.smartContract.on(filterForBridgePayoutEvents, this.handleSmartBridgePayouts.bind(this));

		// TODO: Properly determine request requirements and show the right one here.
		await setBridgeRequirement('CONFIRMATIONS', '0');

		// Set the Bridge Request Payout Amount to the actual amount received into our frontend wallet.
		// NOTE: User can overpay intended amount when using BCH.
		this.bridgeRequest.bridgeAmount = accountBalance / SATOSHIS_PER_BITCOIN_CASH;

		// Internally mark this request as the currently pending bridge request.
		await this.markRequestAsPending(bridgeTransactionHash);

		// Show the amount forwarded to the user.
		await setRequestAmountReceived(this.bridgeRequest.bridgeAmount);

		// Show the request transaction hash to the user.
		await setRequestBridgeTransaction(bridgeTransactionHash);

		// Prevent users from leaving the request screen until the bridging has completed.
		// TODO: Uncomment below once we identify conditions where completed Bridge Transactions are not recognized by frontend.
		// await disableRequestButton();

		// Show the request follow-up interface.
		await showRequestScreen();

		// Check if there is enough Liquidity to complete pending request.
		await this.updatePendingRequestLiquidityStatus();
	}

	async bridgeToCash(payoutAddress: string, amount: number): Promise<void>
	{
		// Get the current gas price from the users wallet.
		const gasPrice = await this.smartProvider.getGasPrice();

		// Convert the provided BCH amount to an EVM compatible number.
		const value = ethers.utils.parseUnits(amount.toString());

		// Create an alias for the transaction receiving address for legibility.
		const to = this.smartReceivingAddress;

		// Encode the payout address
		const data = ethers.utils.hexlify(ethers.utils.toUtf8Bytes(payoutAddress));

		try
		{
			// Enable the state variable that indicates we are waiting for the transaction from the Smart Wallet.
			// NOTE: This is used elsewhere to prevent the Proposal Button being enabled by other events/code..
			this.isWaitingForWalletTransaction = true;

			// Disable the Proposal Button while we wait for the transaction to send.
			await disableProposalButton();

			// Set the text where the Time Estimate is on the button to indicate we are waiting for the wallet.
			await updateBridgeButtonTimeEstimate('WAITING_FOR_TRANSACTION');

			// Request the user to send the bridge request.
			const sendResponse = await this.smartSigner.sendTransaction({ to, value, data, gasPrice });

			// Extract the transaction hash for legibility.
			const transactionHash = sendResponse.hash;

			// Internally mark this request as the currently pending bridge request.
			await this.markRequestAsNew(SMART_TO_CASH, amount, payoutAddress);
			//
			await setRequestBridgeTransaction(transactionHash.substring(2));

			// Update and show the number of confirmations for this bridge transaction.
			await this.updateCashConfirmations();

			// Set the current request as pending, showing the appropriate presentation.
			await this.markRequestAsPending(transactionHash);

			// Show the amount that was received to the user.
			await setRequestAmountReceived(amount);

			// Prevent users from leaving the request screen until the bridging has completed.
			// TODO: Uncomment below once we identify conditions where completed Bridge Transactions are not recognized by frontend.
			// await disableRequestButton();

			// Show the request follow-up interface.
			await showRequestScreen();

			// Check if there is enough Liquidity to complete pending request.
			await this.updatePendingRequestLiquidityStatus();

			// Update the confirmation when it gets included in a block.
			// NOTE: This is run async to be non-blocking.
			this.updateSmartConfirmationOnBlockInclusion(transactionHash);
		}
		catch(error)
		{
			// TODO: Add proper logging mechanics instead of using the console log.
			console.log('Failed to send transaction:', error);
		}
		finally
		{
			// Disable our state variable that indicates a transaction is sending.
			this.isWaitingForWalletTransaction = false;

			// Determine whether the Proposal Button should be re-enabled.
			await this.validateBridgeRequest();

			// Revert the time estimate on the button to its original value.
			await this.estimateBridgeSpeed();
		}
	}

	async updateSmartConfirmationOnBlockInclusion(transactionHash: string): Promise<void>
	{
		// Get a transaction response from the network..
		const transactionResponse = await this.smartProvider.getTransaction(transactionHash);

		// Wait for the transaction to be included in a block.
		// NOTE: this can reject if transaction is reverted.
		// TODO: Handle revert cases.
		await transactionResponse.wait();

		// Update the requirement presentation to mark the bridge transaction as complete.
		await setBridgeRequirement('CONFIRMATIONS', '1/1');
	}

	/**
	 * Gets the number of confirmations for the pending Cash to Smart Bridge Request (if there is one active).
	 */
	async updateCashConfirmations(): Promise<void>
	{
		// Return if no active bridge request exists.
		if(!this.bridgeRequest)
		{
			return;
		}

		// Evaluate conditions that should make us skip setting bridge confirmations.
		const isNotCashToSmart = this.bridgeRequest.bridgeDirection !== CASH_TO_SMART;
		const isMissingSourceTransactionHash = !this.bridgeRequest.sourceTransactionHash;
		const isPaidOut = this.bridgeRequest.payoutTransactionHash;

		// Return if any of the above "skip conditions" are true.
		if(isNotCashToSmart || isMissingSourceTransactionHash || isPaidOut)
		{
			return;
		}

		// Get the current block height.
		const currentHeight = await getCurrentHeight(this.cashProvider);

		// Get the height of our Bridge Transaction.
		const transactionHeight = await getTransactionHeight(this.cashProvider, this.bridgeRequest.sourceTransactionHash as string);

		// Get the required number of confirmations for the given amount (converted to satoshi units).
		const requiredConfirmations = await getRequiredCashConfirmations(Math.round(this.bridgeRequest.bridgeAmount * SATOSHIS_PER_BITCOIN_CASH));

		// We only want to show required confirmations if it is more than zero.
		// Otherwise, if we show "Confirmations 0/0", we will be setting unrealistic expectations where we cannot use a Double-Spend-Proof.
		// So we define our suffix string here and set it to blank if no confirmations required.
		// Or "/${requiredConfirmations}" if confirmations are required based on the given amount.
		const requiredConfirmationsSuffix = (requiredConfirmations) ? `/${requiredConfirmations}` : '';

		// If the transaction height is 0, our transaction is still in the mempool.
		if(transactionHeight === 0)
		{
			// Set confirmations to zero as the Bridge Transaction is still in mempool.
			const confirmations = 0;

			// Update the UI to show zero confirmations.
			await setBridgeRequirement('CONFIRMATIONS', `${confirmations}${requiredConfirmationsSuffix}`);
		}
		// Otherwise, deduct current height from transaction height and add 1 to get number of confirmations.
		else
		{
			// Calculate confirmations by deducting transaction height from current height and then add one.
			// NOTE: We add one because current height minus transaction height equals zero (but indicates one confirmation).
			const confirmations = currentHeight - transactionHeight + 1;

			// Update the UI to show the number of confirmations.
			await setBridgeRequirement('CONFIRMATIONS', `${confirmations}${requiredConfirmationsSuffix}`);
		}
	}

	async copyInputToClipboard(inputElement: HTMLInputElement): Promise<void>
	{
		// Select the text to copy.
		inputElement.select();
		inputElement.setSelectionRange(0, 9999);

		// Copy the input elements value to the clipboard.
		await navigator.clipboard.writeText(inputElement.value);

		// Inform the user that the text has been copied.
		alert(`Copied: ${inputElement.value}`);
	}

	/**
	 * Delay execution of the code for a given number of milliseconds.
	 *
	 * @param milliseconds {number} The number of milliseconds to delay.
	 */
	async delay(milliseconds: number): Promise<void>
	{
		// Create a promise resolve function.
		const timeoutResolver = async function(resolve: Function): Promise<void>
		{
			// Create a function to called once our timeout triggers.
			const timeoutCallback = function(): void
			{
				// Resolve our promise.
				resolve();
			};

			// Set the timeout to call our callback after the given number of milliseconds.
			setTimeout(timeoutCallback, milliseconds);
		};

		// Create our timeout promise.
		const timeoutPromise: Promise<void> = new Promise(timeoutResolver);

		// Return our timeout promise.
		return timeoutPromise;
	}
}

// Create and instance of the Hop Cash Frontend.
/* eslint-disable no-new */
new HopCashFrontend();
