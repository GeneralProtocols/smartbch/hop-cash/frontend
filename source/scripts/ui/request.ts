// Import our translation functions.
import { INNER_TEXT_TRANSLATION_ATTRIBUTE, lookupTranslation } from './language';

// Form element.
export const requestForm = document.querySelector('#request > form')! as HTMLFormElement;

// Status presentation related elements.
export const requestStatus = document.querySelector('#requestStatus')! as HTMLElement;
export const requestStatusIcon = document.querySelector('#requestStatus > div > span > .statusIcon')! as HTMLElement;
export const requestStatusRing = document.querySelector('#requestStatus > div > span > .statusRing')! as HTMLElement;

// Transaction related elements.
export const requestAmountReceived = document.querySelector('#requestAmountReceived')! as HTMLElement;
export const requestTransactionToBridge = document.querySelector('#requestTransactionToBridge')! as HTMLElement;
export const requestTransactionToBridgeInput = document.querySelector('#requestTransactionToBridge > div > input')! as HTMLInputElement;
export const requestTransactionFromBridge = document.querySelector('#requestTransactionFromBridge')! as HTMLElement;
export const requestTransactionFromBridgeInput = document.querySelector('#requestTransactionFromBridge > div > input')! as HTMLInputElement;

// Insufficient Liquidity related elements.
export const requestInsufficientLiquidityNotice = document.querySelector('#requestInsufficientLiquidityNotice')! as HTMLElement;

// Confirmation related elements.
export const requestBridgeRequirement = document.querySelector('#requestBridgeRequirement')! as HTMLElement;
export const requestBridgeState = document.querySelector('#requestBridgeState')! as HTMLElement;

// Promotion link.
export const promotionLink = document.querySelector('#promotionLink')! as HTMLElement;

// Action button related elements.
export const requestCompleteButton = document.querySelector('#completeRequest > button')! as HTMLButtonElement;

/**
 * Disables tabbing to all elements in this view.
 */
export const disableTabbingToRequestElements = async function(): Promise<void>
{
	requestTransactionToBridgeInput.setAttribute('tabindex', '-1');
	requestTransactionFromBridgeInput.setAttribute('tabindex', '-1');
	requestCompleteButton.setAttribute('tabindex', '-1');
};

/**
 * Enables tabbing to all elements in this view.
 */
export const enableTabbingToRequestElements = async function(): Promise<void>
{
	requestTransactionToBridgeInput.removeAttribute('tabindex');
	requestTransactionFromBridgeInput.removeAttribute('tabindex');
	requestCompleteButton.removeAttribute('tabindex');
};

/**
 * Enables and marks the request completion button as valid
 */
export const enableRequestButton = async function(): Promise<void>
{
	// Mark the input as valid.
	requestCompleteButton.classList.remove('invalid');
	requestCompleteButton.classList.add('valid');

	// Make the button accessible.
	requestCompleteButton.disabled = false;
};

/**
 * Disables and marks the request completion button as invalid
 */
export const disableRequestButton = async function(): Promise<void>
{
	// Mark the input as invalid.
	requestCompleteButton.classList.remove('valid');
	requestCompleteButton.classList.add('invalid');

	// Make the button non-interactive.
	requestCompleteButton.disabled = true;
};

/**
 * Updated the visual status indicator.
 */
export const updateRequestStatus = async function(status: '' | 'pending' | 'complete' | 'insufficientLiquidity', rotate?: boolean): Promise<void>
{
	if(rotate)
	{
		requestStatusRing.classList.add('rotating');
	}
	else
	{
		requestStatusRing.classList.remove('rotating');
	}

	// Update the status ring.
	requestStatus.className = status;

	// If the status is insufficient liquidity, show additional UI message.
	if(status === 'insufficientLiquidity')
	{
		requestInsufficientLiquidityNotice.style.display = 'block';
	}
	else
	{
		requestInsufficientLiquidityNotice.style.display = 'none';
	}
};

/**
 * Shows the amount received in the follow-up screen.
 */
export const setRequestAmountReceived = async function(amount: number): Promise<void>
{
	// Show all 8 decimal places to avoid rounding errors and confusing the user.
	const amountAsString = amount.toFixed(8);

	// Show the bridge transaction amount.
	requestAmountReceived.innerText = amountAsString;
};

/**
 * Shows the request bridge transaction in the follow-up screen.
 */
export const setRequestBridgeTransaction = async function(transaction: string): Promise<void>
{
	// Show the bridge transaction hash, and set the title so it also shows as a tooltip on hover.
	requestTransactionToBridgeInput.value = transaction;
	requestTransactionToBridgeInput.title = transaction;
};

/**
 * Shows the request payout transaction in the follow-up screen.
 */
export const setRequestPayoutTransaction = async function(transaction: string): Promise<void>
{
	// Show the bridge payout transaction hash, and set the title so it also shows as a tooltip on hover.
	requestTransactionFromBridgeInput.value = transaction;
	requestTransactionFromBridgeInput.title = transaction;
};

/**
 * Updates the bridge transaction requirements shown to the user.
 */
export const setBridgeRequirement = async function(nameTranslationKey: string, state: string): Promise<void>
{
	// Get the translation string for name based on given key.
	// NOTE: We set a blank space character if no translation available to preserve DOM #text element.
	const name = await lookupTranslation(nameTranslationKey) || ' ';

	// Set the translation attribute so that element updates correctly when user switches language.
	requestBridgeRequirement.setAttribute(INNER_TEXT_TRANSLATION_ATTRIBUTE, nameTranslationKey);

	// Update the inner HTML with our translated string.
	requestBridgeRequirement.innerHTML = name;

	// Set the number of confirmations (e.g. "0/2").
	requestBridgeState.innerHTML = state;
};

/**
 *
 */
export const clearRequestForm = async function(): Promise<void>
{
	// Reset any existing status to default.
	await updateRequestStatus('', false);

	// Clear the bridge and payout transactions.
	await setRequestBridgeTransaction('');
	await setRequestPayoutTransaction('');

	// Remove bridge requirements.
	await setBridgeRequirement('', '');

	// Reset button validity.
	await enableRequestButton();
};
