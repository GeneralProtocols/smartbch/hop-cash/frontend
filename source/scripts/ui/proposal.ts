// Import our translation constants and functions.
import { INNER_TEXT_TRANSLATION_ATTRIBUTE, CUSTOM_VALIDITY_TRANSLATION_ATTRIBUTE, lookupTranslation } from './language';

// Hardcoded names and logos.
export const cashLogo = 'bitcoincash.png';
export const cashName = 'Bitcoin&nbsp;Cash';
export const smartLogo = 'smartbch.png';
export const smartName = 'Smart&nbsp;BCH';

// Form element
export const proposalForm = document.querySelector('#proposal > form')! as HTMLFormElement;

// Direction related elements.
export const proposalDirection = document.querySelector('#proposalDirection')! as HTMLElement;
export const proposedDirectionToSmart = document.querySelector('#proposedDirectionToSmart')! as HTMLInputElement;
export const proposedDirectionToCash = document.querySelector('#proposedDirectionToCash')! as HTMLInputElement;
export const proposedFromLogo = document.querySelector('#proposedFromDirection > button > img')! as HTMLImageElement;
export const proposedFromName = document.querySelector('#proposedFromDirection > button > span')! as HTMLElement;
export const proposedToLogo = document.querySelector('#proposedToDirection > button > img')! as HTMLImageElement;
export const proposedToName = document.querySelector('#proposedToDirection > button > span')! as HTMLElement;

// Amount related elements
export const proposedBridgeAmount = document.querySelector('#proposedBridgeAmount > input')! as HTMLInputElement;
export const proposalMinBridgeAmount = document.querySelector('#minBridgeAmount')! as HTMLElement;
export const proposalMaxBridgeAmount = document.querySelector('#maxBridgeAmount')! as HTMLElement;

// Address related elements.
export const proposedPayoutAddress = document.querySelector('#proposedPayoutAddress > input')! as HTMLInputElement;
export const proposalPayoutAddressNote = document.querySelector('#proposedPayoutAddress > small')! as HTMLElement;

// Action button related elements.
export const submitBridgeRequestButton = document.querySelector('#confirmBridgeRequest > button')! as HTMLButtonElement;
export const selectedBridgeAmount = document.querySelector('#selectedBridgeAmount')! as HTMLElement;
export const bridgeEstimatedTime = document.querySelector('#bridgeEstimatedTime')! as HTMLElement;

/**
 * Disables tabbing to all elements in this view.
 */
export const disableTabbingToProposalElements = async function(): Promise<void>
{
	proposedDirectionToSmart.setAttribute('tabindex', '-1');
	proposedDirectionToCash.setAttribute('tabindex', '-1');
	proposedBridgeAmount.setAttribute('tabindex', '-1');
	proposedPayoutAddress.setAttribute('tabindex', '-1');
	submitBridgeRequestButton.setAttribute('tabindex', '-1');
};

/**
 * Enables tabbing to all elements in this view.
 */
export const enableTabbingToProposalElements = async function(): Promise<void>
{
	proposedDirectionToSmart.removeAttribute('tabindex');
	proposedDirectionToCash.removeAttribute('tabindex');
	proposedBridgeAmount.removeAttribute('tabindex');
	proposedPayoutAddress.removeAttribute('tabindex');
	submitBridgeRequestButton.removeAttribute('tabindex');
};

/**
 * Enables and marks the bridge proposal button as valid
 */
export const enableProposalButton = async function(): Promise<void>
{
	// Mark the input as valid.
	submitBridgeRequestButton.classList.remove('invalid');
	submitBridgeRequestButton.classList.add('valid');

	// Make the button accessible.
	submitBridgeRequestButton.disabled = false;
};

/**
 * Disables and marks the bridge proposal button as invalid
 */
export const disableProposalButton = async function(): Promise<void>
{
	// Mark the input as invalid.
	submitBridgeRequestButton.classList.remove('valid');
	submitBridgeRequestButton.classList.add('invalid');

	// Make the button non-interactive.
	submitBridgeRequestButton.disabled = true;
};

/**
 * Returns the current bridge amount proposed by the user.
 */
export const getProposedBridgeAmount = async function(): Promise<number>
{
	return Number(proposedBridgeAmount.value);
};

/**
 * Marks the bridge amount as valid.
 */
export const markProposedBridgeAmountAsValid = async function(): Promise<void>
{
	// Remove any custom invalidation reason.
	proposedBridgeAmount.setCustomValidity('');

	// Mark the input as valid.
	proposedBridgeAmount.classList.remove('invalid');
	proposedBridgeAmount.classList.add('valid');
};

/**
 * Marks the bridge amount as invalid with an optional reason.
 *
 * @param reason    optional reason to display in the browser.
 */
export const markProposedBridgeAmountAsInvalid = async function(reason: string = ''): Promise<void>
{
	// Set a reason shown by the browser if provided.
	proposedBridgeAmount.setCustomValidity(reason);

	// Mark the input as invalid.
	proposedBridgeAmount.classList.remove('valid');
	proposedBridgeAmount.classList.add('invalid');
};

/**
 * Updates presentation for amount validity.
 */
export const updateProposalBridgeAmountValidity = async function(): Promise<void>
{
	const bridgeAmount = proposedBridgeAmount.value;

	if(!bridgeAmount)
	{
		await markProposedBridgeAmountAsValid();
	}
	else
	{
		const bridgeAmountIsValid = proposedBridgeAmount.validity.valid;

		if(bridgeAmountIsValid)
		{
			await markProposedBridgeAmountAsValid();
		}
		else
		{
			await markProposedBridgeAmountAsInvalid();
		}
	}
	//
	const bridgeRequestIsValid = submitBridgeRequestButton.form!.checkValidity();

	//
	if(bridgeRequestIsValid)
	{
		await enableProposalButton();
	}
	else
	{
		await disableProposalButton();
	}
};

/**
 * Shows a user friendly max amount, and limits the user input to the actual max amount.
 *
 * @note the actual max amount is limited by the divisibility of the BCH network, so set to 8 decimals.
 */
export const showAndLimitMaxBridgeAmount = async function(amount: number): Promise<void>
{
	// Limit the max value for user amount input.
	proposedBridgeAmount.setAttribute('max', amount.toFixed(8));

	// Show the max value to the user.
	proposalMaxBridgeAmount.innerHTML = amount.toFixed(2);
};

/**
 * Sets the user amount input to the minimum allowed.
 */
export const setProposedAmountToMinimum = async function(): Promise<void>
{
	proposedBridgeAmount.value = proposedBridgeAmount.min;
};

/**
 * Sets the user amount input to the maximum allowed.
 */
export const setProposedAmountToMaximum = async function(): Promise<void>
{
	proposedBridgeAmount.value = proposedBridgeAmount.max;
};

/**
 * Returns the current bridge payout address proposed by the user.
 */
export const getProposedPayoutAddress = async function(): Promise<string>
{
	return proposedPayoutAddress.value;
};

/**
 * Sets or removes the payout address presented on the bridge view, and if provided also makes it read only.
 *
 * @param address   payout address used for the next bridge request.
 */
export const setProposedPayoutAddress = async function(address?: string): Promise<void>
{
	// Set and ensure the payout address is read-only, if an address was provided.
	// Also show set the title so that address is shown in a tooltip on hover.
	if(address)
	{
		proposedPayoutAddress.value = address;
		proposedPayoutAddress.title = address;
		proposedPayoutAddress.readOnly = true;
	}
	// Reset the payout address and allow user editing, if none was provided.
	else
	{
		proposedPayoutAddress.value = '';
		proposedPayoutAddress.title = '';
		proposedPayoutAddress.readOnly = false;
	}
};

/**
 * Shows the Payout Address Note.
 */
export const showPayoutAddressNote = async function(): Promise<void>
{
	// Make the payout note visible by setting opacity to 1.
	proposalPayoutAddressNote.style.opacity = '1';
};

/**
 * Hides the Payout Address Note.
 */
export const hidePayoutAddressNote = async function(): Promise<void>
{
	// Make the payout note invisible by setting opacity to 0.
	// NOTE: Note we use this approach to prevent the UI jumping around.
	proposalPayoutAddressNote.style.opacity = '0';
};

/**
 * Marks the bridge payout address as valid.
 */
export const validateProposedBridgePayoutAddress = async function(): Promise<void>
{
	// Remove any custom invalidation reason.
	proposedPayoutAddress.setCustomValidity('');

	// Set the translation attribute to be empty so that element does not show as invalid when user switches language.
	bridgeEstimatedTime.setAttribute(CUSTOM_VALIDITY_TRANSLATION_ATTRIBUTE, '');

	// Check the validity (this needs to be forcefully updated).
	proposedPayoutAddress.checkValidity();

	// Mark the input as valid.
	proposedPayoutAddress.classList.remove('invalid');
	proposedPayoutAddress.classList.add('valid');
};

/**
 * Marks the bridge payout address as invalid with an optional reason.
 *
 * @param reason    optional reason to display in the browser.
 */
export const invalidateProposedBridgePayoutAddress = async function(translationKey: string = ''): Promise<void>
{
	// Get our translated text based on the translation key provided.
	const reason = await lookupTranslation(translationKey);

	// Set a reason shown by the browser if provided.
	proposedPayoutAddress.setCustomValidity(reason);

	// Trigger the validity check only if there is a value in the field (we do not want this to show if empty).
	// NOTE: We need to do this because it will not fire until our submit button is clicked otherwise.
	//       And our button is in a disabled state, meaning this would never be able to show.
	if(proposedPayoutAddress.value.length)
	{
		proposedPayoutAddress.reportValidity();
	}

	// Set the translation attribute so that element updates correctly when user switches language.
	bridgeEstimatedTime.setAttribute(CUSTOM_VALIDITY_TRANSLATION_ATTRIBUTE, translationKey);

	// Mark the input as invalid.
	proposedPayoutAddress.classList.remove('valid');
	proposedPayoutAddress.classList.add('invalid');
};

/**
 * Updates the shown amount on the bridge button, based on the user input.
 */
export const updateBridgeButtonAmount = async function(): Promise<void>
{
	selectedBridgeAmount.innerHTML = Number(proposedBridgeAmount.value).toFixed(2);
};

/**
 * Updates the shown time estimate on the bridge button.
 *
 * @param estimatedTime   string presented to the user as a time estimate for the request.
 */
export const updateBridgeButtonTimeEstimate = async function(translationKey: string): Promise<void>
{
	// Look up our translation based on the Translation Key provided.
	bridgeEstimatedTime.innerHTML = await lookupTranslation(translationKey);

	// Set the translation attribute so that element updates correctly when user switches language.
	bridgeEstimatedTime.setAttribute(INNER_TEXT_TRANSLATION_ATTRIBUTE, translationKey);
};

/**
 * Shows the bridge button as valid or invalid based on form validity.
 */
export const updateProposalButtonValidity = async function(): Promise<void>
{
	//
	const bridgeRequestIsValid = submitBridgeRequestButton.form!.checkValidity();

	//
	if(bridgeRequestIsValid)
	{
		await enableProposalButton();
	}
	else
	{
		await disableProposalButton();
	}
};

/**
 * Returns true if the currently selected direction is smartToCash.
 */
export const proposedDirectionIsToCash = async function(): Promise<boolean>
{
	return !!proposedDirectionToCash.checked;
};

/**
 * Returns true if the currently selected direction is cashToSmart.
 *
 * @note this relies on the browser to properly enforce that the two direction radio buttons cannot be enabled at the same time.
 */
export const proposedDirectionIsToSmart = async function(): Promise<boolean>
{
	return !!proposedDirectionToSmart.checked;
};

/**
 * Sets the presented direction from SmartBCH to Bitcoin Cash.
 */
export const showDirectionToCash = async function(): Promise<void>
{
	// New direction is smart2cash.
	proposedDirectionToCash.checked = true;

	// Update logos and names.
	proposedFromLogo.src       = smartLogo;
	proposedFromName.innerHTML = smartName;
	proposedToLogo.src         = cashLogo;
	proposedToName.innerHTML   = cashName;

	// Update input name to match direction.
	// NOTE: Allows browsers to keep separate history for each direction.
	proposedPayoutAddress.setAttribute('name', 'bridgeCashPayoutAddress');

	// Clear the payout address field, which will also make it editable.
	await setProposedPayoutAddress();

	// Remove address metamask note.
	await hidePayoutAddressNote();
};

/**
 * Sets the presented direction from Bitcoin Cash to Smart BCH.
 */
export const showDirectionToSmart = async function(address: string): Promise<void>
{
	// New direction is smart2cash.
	proposedDirectionToSmart.checked = true;

	// Update logos and names.
	proposedFromLogo.src       = cashLogo;
	proposedFromName.innerHTML = cashName;
	proposedToLogo.src         = smartLogo;
	proposedToName.innerHTML   = smartName;

	// Update input name to match direction.
	// NOTE: Allows browsers to keep separate history for each direction.
	proposedPayoutAddress.setAttribute('name', 'bridgeSmartPayoutAddress');

	// Initialize to the metamask address, which will set it to read-only to prevent user changes.
	await setProposedPayoutAddress(address);

	// Show address metamask note.
	await showPayoutAddressNote();
};

/**
 * Resets the proposal form to its default direction, amount and address states.
 */
export const clearProposalForm = async function(address: string): Promise<void>
{
	// Clear the proposal value.
	proposedBridgeAmount.value = '';

	// Update the proposal button amount text.
	await updateBridgeButtonAmount();

	// Update the proposal speed estimate.
	await updateBridgeButtonTimeEstimate('CANNOT_BRIDGE_LESS_THAN_MINIMUM_AMOUNT');

	// Reset the proposal direction to default.
	await showDirectionToSmart(address);

	// Revalidate the form.
	await updateProposalButtonValidity();
};
