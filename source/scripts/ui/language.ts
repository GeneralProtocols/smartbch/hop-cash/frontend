// Import our storage functionality.
// NOTE: This will attempt to use IndexedDB but fallback to an in-memory store if not available.
import { get, set } from '../storage';

// Import our translation dictionary.
import translations from '../../translations.json';

// Name of HTML attribute to use for Inner Text translations.
export const INNER_TEXT_TRANSLATION_ATTRIBUTE = 'data-translation';

// Name of HTML attribute to use for Input Placeholder translations.
export const PLACEHOLDER_TRANSLATION_ATTRIBUTE = 'data-translation-placeholder';

// Name of HTML attribute to use for Input Custom Validity translations.
export const CUSTOM_VALIDITY_TRANSLATION_ATTRIBUTE = 'data-translation-custom-validity';

// Name of HTML attribute to use for dynamic link (href) elements that should change depending on language.
export const DYNAMIC_LINK_TRANSLATION_ATTRIBUTE = 'data-translation-href';

// Language Button Elements.
export const languageButtons = Array.from(document.querySelectorAll('[data-set-language]')!) as Array<HTMLElement>;

// The different kinds of translatable elements supported.
// NOTE: We use Array.from() because NodeListOf is not recognized by eslint and also does not support ES6 array functions..
export const translatableInnerTextElements = Array.from(document.querySelectorAll(`[${INNER_TEXT_TRANSLATION_ATTRIBUTE}]`)) as Array<HTMLElement>;
export const translatablePlaceholders = Array.from(document.querySelectorAll(`[${PLACEHOLDER_TRANSLATION_ATTRIBUTE}]`)) as Array<HTMLInputElement>;
export const translatableCustomValidity = Array.from(document.querySelectorAll(`[${CUSTOM_VALIDITY_TRANSLATION_ATTRIBUTE}]`)) as Array<HTMLInputElement>;
export const dynamicLinkElements = Array.from(document.querySelectorAll(`[${DYNAMIC_LINK_TRANSLATION_ATTRIBUTE}]`)) as Array<HTMLInputElement>;

// Create a type for our supported languages.
export type SupportedLanguageCodes = 'en' | 'cn' | 'es';

/**
 * Translates a given key into the currently active language.
 *
 * @param translationKey {string} The translation key to look up in our dictionary.
 */
export const lookupTranslation = async function(translationKey: string): Promise<string>
{
	// If we receive an empty or undefined translation key, we should just return an empty string.
	// NOTE: This is expected as Custom Validity attributes may have no errors set.
	if(!translationKey)
	{
		return '';
	}

	// Get the language code from IndexedDB.
	const languageCode = await get('language') as SupportedLanguageCodes;

	// Read a list of our translation phrases given a language code.
	const translation = translations[languageCode] as Record<string, string>;

	// Get the translated phrase for the given key.
	const translatedPhrase = translation[translationKey];

	// If we do not have a translated phrase for the given key, log a warning so we know to add it.
	if(!translatedPhrase)
	{
		// Log a warning to console so that we know we need to add a translation for this key.
		console.warn(`Translation Error: ${translationKey} is not defined for ${languageCode}`);

		// Return the translation key name so we know where it is on our DOM.
		return translationKey;
	}

	return translatedPhrase;
};

/**
 * Iterates all supported Translation Elements and converts to the given language code.
 *
 * @param languageCode {SupportedLanguageCodes} The language code to use.
 */
export const translateUserInterface = async function(languageCode: SupportedLanguageCodes): Promise<void>
{
	// Store the chosen language in IndexedDB.
	await set('language', languageCode);

	// Remove the selected class from all our language buttons.
	for(const languageButton of languageButtons)
	{
		languageButton.classList.remove('selected');
	}

	// Get the language button that matches the language we are setting to.
	const languageButton = document.querySelector(`[data-set-language="${languageCode}"]`) as HTMLElement;

	// Add the class "selected" to that language button.
	languageButton.classList.add('selected');

	// Translate our inner-text elements.
	// NOTE: To prevent breaking event listeners, etc, our inner text translations do not modify the DOM structure.
	//       Instead we indicate where to split our translation strings for a Child DOM element using "{{ placeholder }}" in our translation strings.
	//       For example: "Amount {{ 0.05 }} BCH" represents the DOM "Amount <span id="amount">0.01</span> BCH".
	//       If we need to modify a child DOM element's text, it must have its own explicit translation string.
	for(const element of translatableInnerTextElements)
	{
		// Get the key we should use for this translation.
		const translationKey = element.getAttribute(INNER_TEXT_TRANSLATION_ATTRIBUTE) as string;

		// Get the corresponding translation for the given key.
		const translatedPhrase = await lookupTranslation(translationKey);

		// Split our translated phrase where there are "{{ }}" placeholders so that our translations can "jump over" child DOM elements.
		const translatedPhraseParts = translatedPhrase.split(new RegExp('{{[^}^}]*}}', 'g'));

		// Get our child elements.
		const childElements = Array.from(element.childNodes);

		// Filter for those nodes that are of type #text (i.e. those elements that are textual and do not modify DOM structure).
		const childTextElements = childElements.filter((node) => node.nodeName === '#text');

		// Make sure that the number of translation parts is equal to the number of textual child nodes.
		if(translatedPhraseParts.length !== childTextElements.length)
		{
			console.log(`Translation Error: ${translationKey} parts (${translatedPhraseParts.length}) is not equal to textual child nodes (${childTextElements.length}).`);
		}

		// Set the translation on each of the textual child nodes.
		for(const index in childTextElements)
		{
			// Get the node from the index.
			const node = childTextElements[index];

			// Set the text content to our translation part at the same index.
			// NOTE: If part does not exist at index, we set a blank space to preserve our child text element.
			node.textContent = translatedPhraseParts[index] || ' ';
		}
	}

	// Translate our placeholder elements.
	for(const element of translatablePlaceholders)
	{
		// Get the key we should use for this translation.
		const translationKey = element.getAttribute(PLACEHOLDER_TRANSLATION_ATTRIBUTE) as string;

		// Get the corresponding translation for the given key.
		const translatedPhrase = await lookupTranslation(translationKey);

		// Set the placeholder text.
		element.setAttribute('placeholder', translatedPhrase);
	}

	// Translate our Custom Validity Inputs.
	for(const element of translatableCustomValidity)
	{
		// Get the key we should use for this translation.
		const translationKey = element.getAttribute(CUSTOM_VALIDITY_TRANSLATION_ATTRIBUTE) as string;

		// Get the corresponding translation for the given key.
		const translatedPhrase = await lookupTranslation(translationKey);

		// Set the placeholder text.
		element.setCustomValidity(translatedPhrase);
	}

	// Modify any pertinent dynamic links (href) depending on language selected.
	for(const element of dynamicLinkElements)
	{
		// Get the key we should use for this translation.
		const translationKey = element.getAttribute(DYNAMIC_LINK_TRANSLATION_ATTRIBUTE) as string;

		// Get the corresponding translation for the given key.
		const translatedPhrase = await lookupTranslation(translationKey);

		// Set the placeholder text.
		element.setAttribute('href', translatedPhrase);
	}
};

/**
 * Initializes translations by:
 * 1. Binding language button click listeners.
 * 2. Getting the last language used from IndexedDB (or defaults to English).
 * 3. And then translate the User Interface into that language.
 */
export const initializeTranslationSupport = async function(): Promise<void>
{
	// Add click listener for each language button.
	for(const languageButton of languageButtons)
	{
		// Get the language code that this button should set.
		const languageTheButtonShouldSet = languageButton.getAttribute('data-set-language') as SupportedLanguageCodes;

		// Add a click event listener that will set this language code when clicked.
		languageButton.addEventListener('click', () =>
		{
			translateUserInterface(languageTheButtonShouldSet);
		});
	}

	// Get the last known browser language from IndexedDB.
	// NOTE: On first visit, this will return 'undefined', which is intended.
	const previousLanguageCode = await get('browserLanguage');

	// Get the current browser language code.
	// NOTE: we split the string by the first hyphen (-) in order to get the primary language tag.
	// https://en.wikipedia.org/wiki/IETF_language_tag#Syntax_of_language_tags
	const currentBrowserLanguageCode = navigator.language.split('-')[0];

	// Force a reset of, and use the browser language, only when it has changed or on first visit.
	if(currentBrowserLanguageCode !== previousLanguageCode)
	{
		// Store the current browser language code.
		await set('browserLanguage', currentBrowserLanguageCode);

		// Store the current browser language as the user interface language of choice.
		// NOTE: We need to translate ZH to CN to match previous shorthands and translation file keys.
		// NOTE: We only support three languages (en, es, cn), and default to english for all other cases.
		if(currentBrowserLanguageCode.toLowerCase() === 'zh')
		{
			await set('language', 'cn');
		}
		else if(currentBrowserLanguageCode.toLowerCase() === 'es')
		{
			await set('language', 'es');
		}
		// Use english for all other languages.
		else
		{
			await set('language', 'en');
		}
	}

	// Get the language that the user has selected previously from our IndexedDB - otherwise use English.
	const language = await get('language') as SupportedLanguageCodes ?? 'en';

	// Translate the User Interface into the selected language.
	await translateUserInterface(language);
};
