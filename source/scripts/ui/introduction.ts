// Form element
export const introductionForm = document.querySelector('#introduction > form')! as HTMLFormElement;

// Action button related elements.
export const closeIntroductionButton = document.querySelector('#closeIntroduction > button')! as HTMLButtonElement;
