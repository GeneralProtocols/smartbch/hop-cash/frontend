// Import our translation constants and functions.
import { CUSTOM_VALIDITY_TRANSLATION_ATTRIBUTE, lookupTranslation } from './language';

// Import libraries to manage QR codes.
import { qrcode, svg2url } from 'pure-svg-code';

// Form element
export const paymentForm = document.querySelector('#payment > form')! as HTMLFormElement;

// Payment link and code related elements.
export const paymentLink = document.querySelector('#paymentLink > div > a')! as HTMLAnchorElement;
export const paymentQrCode = document.querySelector('#paymentLink > img')! as HTMLImageElement;

// Amount related elements.
export const paymentAmount = document.querySelector('#paymentAmount')! as HTMLElement;
export const paymentAmountInput = document.querySelector('#paymentAmount > div > input')! as HTMLInputElement;

// Under-Payment related elements.
export const paymentBelowMinimumAmountNotice = document.querySelector('#paymentBelowMinimumAmountNotice')! as HTMLElement;
export const paymentAmountReceived = document.querySelector('#paymentAmountReceived')! as HTMLElement;

// Address related elements.
export const paymentCashAddress = document.querySelector('#paymentCashAddress')! as HTMLElement;
export const paymentCashAddressInput = document.querySelector('#paymentCashAddress > div > input')! as HTMLInputElement;
export const paymentLegacyAddress = document.querySelector('#paymentLegacyAddress')! as HTMLElement;
export const paymentLegacyAddressInput = document.querySelector('#paymentLegacyAddress > div > input')! as HTMLInputElement;

// Action button related elements.
export const paymentCancelButton = document.querySelector('#cancelPayment > button')! as HTMLButtonElement;

/**
 * Disables tabbing to all elements in this view.
 */
export const disableTabbingToPaymentElements = async function(): Promise<void>
{
	paymentLink.setAttribute('tabindex', '-1');
	paymentAmountInput.setAttribute('tabindex', '-1');
	paymentCashAddressInput.setAttribute('tabindex', '-1');
	paymentLegacyAddressInput.setAttribute('tabindex', '-1');
	paymentCancelButton.setAttribute('tabindex', '-1');
};

/**
 * Enables tabbing to all elements in this view.
 */
export const enableTabbingToPaymentElements = async function(): Promise<void>
{
	paymentLink.removeAttribute('tabindex');
	paymentAmountInput.removeAttribute('tabindex');
	paymentCashAddressInput.removeAttribute('tabindex');
	paymentLegacyAddressInput.removeAttribute('tabindex');
	paymentCancelButton.removeAttribute('tabindex');
};

/**
 * Enables and marks the payment cancel button as valid
 */
export const enablePaymentButton = async function(): Promise<void>
{
	// Mark the input as valid.
	paymentCancelButton.classList.remove('invalid');
	paymentCancelButton.classList.add('valid');

	// Make the button accessible.
	paymentCancelButton.disabled = false;

	// Remove any custom invalidation reason.
	paymentCancelButton.setCustomValidity('');

	// Empty the translation string so that custom validation is not re-inserted when user switches languages.
	paymentCancelButton.setAttribute(CUSTOM_VALIDITY_TRANSLATION_ATTRIBUTE, '');
};

/**
 * Disables and marks the payment cancel button as invalid
 */
export const disablePaymentButton = async function(reasonTranslationKey: string): Promise<void>
{
	// Get our translated text based on the translation key provided.
	const reason = await lookupTranslation(reasonTranslationKey);

	// Set a custom reason for why we are disabling the button.
	paymentCancelButton.setCustomValidity(reason);

	// Set the translation attribute so that element updates correctly when user switches language.
	paymentCancelButton.setAttribute(CUSTOM_VALIDITY_TRANSLATION_ATTRIBUTE, reasonTranslationKey);

	// Mark the input as invalid.
	paymentCancelButton.classList.remove('valid');
	paymentCancelButton.classList.add('invalid');

	// Make the button non-interactive.
	paymentCancelButton.disabled = true;
};

/**
 * Creates a BIP21 compatible payment url from a bitcoin cash address and amount.
 *
 * @param amount    bitcoin cash amount to request for the payment
 * @param address   cashAddress where the payment should be sent.
 */
export const createPaymentUrl = async function(amount: number, address: string): Promise<string>
{
	// Set the payment prefix, if it is missing.
	let paymentPrefix = '';
	if(!address.startsWith('bitcoincash:'))
	{
		paymentPrefix = 'bitcoincash:';
	}

	// Construct the payment URL.
	const paymentUrl = `${paymentPrefix}${address}?amount=${amount}`;

	return paymentUrl;
};

/**
 * Updates the payment link to a new payment URL.
 *
 * @param paymentUrl   a bitcoincash: BIP21 compatible payment URL.
 */
export const updatePaymentLink = async function(paymentUrl: string): Promise<void>
{
	// Update the payment link with the new URL.
	paymentLink.innerHTML = paymentUrl;
	paymentLink.href = paymentUrl;
};

/**
 * Updates the payment QR code to a new payment URL.
 *
 * @param paymentUrl   a bitcoincash: BIP21 compatible payment URL.
 */
export const updatePaymentCode = async function(paymentUrl: string): Promise<void>
{
	// Update the payment QR code.
	const paymentQrCodeSVG = svg2url(qrcode(paymentUrl));

	// Update the QR code image to use the new SVG content.
	paymentQrCode.src = paymentQrCodeSVG;
};

/**
 * Updates the payment links and QR code to a new payment URL.
 *
 * @param amount          bitcoin cash amount to request for the payment
 * @param cashAddress     cashAddress where the payment should be sent.
 * @param legacyAddress   legacy base58 address where the payment should be sent.
 */
export const updatePaymentDetails = async function(amount: number, cashAddress: string, legacyAddress: string): Promise<void>
{
	paymentAmountInput.value = `${amount} BCH`;

	// Show the cash address, and set the title so it also shows as a tooltip on hover.
	paymentCashAddressInput.value = cashAddress.substring(12);
	paymentCashAddressInput.title = cashAddress.substring(12);

	// Show the legacy address, and set the title so it also shows as a tooltip on hover.
	paymentLegacyAddressInput.value = legacyAddress;
	paymentLegacyAddressInput.title = legacyAddress;

	// Figure out payment url to use for the link and QR code.
	const paymentUrl = await createPaymentUrl(amount, cashAddress);

	// Update the QR code to use the payment url.
	await updatePaymentLink(paymentUrl);
	await updatePaymentCode(paymentUrl);
};

/**
 * Update the amount that the user has paid into the frontend wallet.
 *
 * @param amount {number} The amount in Cash (BCH) Units.
 */
export const updatePaymentAmountReceived = async function(amount: number) : Promise<void>
{
	// Show all 8 decimal places.
	// NOTE: If we attempt to truncate this shorter, we can end up with unusual Javascript formatting errors.
	//       For example, (0.0099).toFixed(3) will output as 0.010.
	paymentAmountReceived.innerText = amount.toFixed(8);
};

/**
 * Shows the cancel button as valid or invalid based on form validity.
 */
export const updatePaymentButtonValidity = async function(): Promise<void>
{
	//
	const cancelPaymentIsValid = paymentCancelButton.form!.checkValidity();

	//
	if(cancelPaymentIsValid)
	{
		await enablePaymentButton();
	}
	else
	{
		await disablePaymentButton('');
	}
};

/**
 * Show notification indicating that the user has under-paid.
 */
export const showPaymentBelowMinimumAmountNotice = async function(): Promise<void>
{
	paymentBelowMinimumAmountNotice.style.display = 'block';
};

/**
 * Hide notification indicating that the user has under-paid.
 */
export const hidePaymentBelowMinimumAmountNotice = async function(): Promise<void>
{
	paymentBelowMinimumAmountNotice.style.display = 'none';
};

/**
 * Reset the payment form to it's default state.
 */
export const clearPaymentForm = async function(): Promise<void>
{
	// Clear the link and qr code, amount and addresses.
	await updatePaymentDetails(0, '', '');

	// Clear the payment button.
	await enablePaymentButton();
};
