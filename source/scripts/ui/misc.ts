// Import utility functions from other views.
import { enableTabbingToProposalElements, disableTabbingToProposalElements } from './proposal';
import { enableTabbingToPaymentElements, disableTabbingToPaymentElements } from './payment';
import { enableTabbingToRequestElements, disableTabbingToRequestElements } from './request';

// Main element controlling what is shown to the user.
export const mainElement = document.querySelector('main')! as HTMLElement;

// Initialize elements that should have Hop.Cash Constants injected into them.
export const bridgeMinimumAmountElements = Array.from(document.querySelectorAll('.bridgeMinimumAmount')!) as Array<HTMLElement>;
export const feePercentageElements = Array.from(document.querySelectorAll('.feePercentage')!) as Array<HTMLElement>;
export const minimumFeeElements = Array.from(document.querySelectorAll('.minimumFee')!) as Array<HTMLElement>;

// Initialize the overlay and related elements.
export const installWallet = document.querySelector('#installWallet')! as HTMLElement;
export const connectWallet = document.querySelector('#connectWallet')! as HTMLElement;
export const unlockWallet = document.querySelector('#unlockWallet')! as HTMLElement;
export const setupNetwork = document.querySelector('#setupNetwork')! as HTMLElement;

// Initialize the footer elements.
export const helpToggle = document.querySelector('#helpToggle')! as HTMLElement;

/**
 * Enables styles that make it easier to debug layout and rendering issues.
 */
export const enableLayoutDebugging = async function(): Promise<void>
{
	document.body.classList.add('debug');
};

/**
 * Disabled styles that make it easier to debug layout and rendering issues.
 */
export const disableLayoutDebugging = async function(): Promise<void>
{
	document.body.classList.remove('debug');
};

export const hideAllScreens = async function(): Promise<void>
{
	// Hide the proposal, payment and request screens.
	mainElement.classList.remove('showProposal');
	mainElement.classList.remove('showPayment');
	mainElement.classList.remove('showRequest');

	// Disable tabbing to hidden screens.
	await disableTabbingToProposalElements();
	await disableTabbingToPaymentElements();
	await disableTabbingToRequestElements();
};

/**
 * Shows the bridge view.
 */
export const showProposalScreen = async function(): Promise<void>
{
	// Hide and make all screen inaccessible by tabbing.
	await hideAllScreens();

	// Enable tabbing to the intended screen.
	await enableTabbingToProposalElements();

	// Show the intended screen to the user.
	mainElement.classList.add('showProposal');
};

/**
 * Shows the payment view.
 */
export const showPaymentScreen = async function(): Promise<void>
{
	// Hide and make all screen inaccessible by tabbing.
	await hideAllScreens();

	// Enable tabbing to the intended screen.
	await enableTabbingToPaymentElements();

	// Show the intended screen to the user.
	mainElement.classList.add('showPayment');
};

/**
 * Shows the request view.
 */
export const showRequestScreen = async function(): Promise<void>
{
	// Hide and make all screen inaccessible by tabbing.
	await hideAllScreens();

	// Enable tabbing to the intended screen.
	await enableTabbingToRequestElements();

	// Show the intended screen to the user.
	mainElement.classList.add('showRequest');
};

/**
 * Shows the requirements overlay.
 */
export const showRequirementsOverlay = async function(): Promise<void>
{
	mainElement.classList.add('showRequirements');
};

/**
 * Hides the requirements overlay.
 */
export const hideRequirementsOverlay = async function(): Promise<void>
{
	mainElement.classList.remove('showRequirements');
};

/**
 * Shows the welcome screen.
 */
export const showIntroduction = async function(): Promise<void>
{
	mainElement.classList.add('showIntroduction');
};

/**
 * Hides the welcome screen.
 */
export const hideIntroduction = async function(): Promise<void>
{
	mainElement.classList.remove('showIntroduction');
};
