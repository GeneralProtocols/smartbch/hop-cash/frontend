// Import a simple key-value storage that uses the IndexedDB feature of modern browsers.
import {
	get as indexedDbGet,
	set as indexedDbSet,
	del as indexedDbDel,
} from 'idb-keyval';

// Flag indicating whether IndexedDB can be used.
let canUseIndexedDb: boolean | undefined;

// If IndexedDB is not supported, we will just store any data in a simple Map Object.
const inMemoryStore: Map<string, any> = new Map();

/**
 * Checks if we can use IndexedDB (and stores the first-run result in a boolean flag so that subsequent calls are quick).
 *
 * @returns {boolean} True if IndexedDB can be used, false otherwise.
 */
const checkIfweCanUseIndexedDb = async function(): Promise<boolean>
{
	// If we have not yet determined whether we can use IndexedDB, do it now.
	if(typeof canUseIndexedDb === 'undefined')
	{
		try
		{
			// Attempt to get any key.
			// NOTE: This will throw an error if IndexedDB is not available.
			await indexedDbGet('testKeyThatShouldNotExist');

			// Set our IndexedDB availability flag to true.
			canUseIndexedDb = true;
		}
		catch(error)
		{
			// Set our IndexedDB availability flag to false.
			canUseIndexedDb = false;

			// Log a warning for debugging purposes.
			console.warn('IndexedDB was not available. Falling back to in-memory storage.');
		}
	}

	// Return our flag inidcating whether IndexedDB can be used.
	return canUseIndexedDb;
};

/**
 * 'get' function that will fallback to in-memory store if IndexedDB is not available.
 *
 * @param key {string} The key to retrieve from our storage.
 */
export const get = async function(key: string): Promise<any>
{
	if(await checkIfweCanUseIndexedDb())
	{
		return indexedDbGet(key);
	}
	else
	{
		return inMemoryStore.get(key);
	}
};

/**
 * 'set' function that will fallback to in-memory store if IndexedDB is not available.
 *
 * @param key   {string} The key to set in our storage.
 * @param value {any}    The value to set.
 */
export const set = async function(key: string, value: any): Promise<void>
{
	if(await checkIfweCanUseIndexedDb())
	{
		indexedDbSet(key, value);
	}
	else
	{
		inMemoryStore.set(key, value)
	}
};

/**
 * 'del' function that will fallback to in-memory store if IndexedDB is not available.
 *
 * @param key   {string} The key to delete in our storage.
 */
export const del = async function(key: string): Promise<void>
{
	if(await checkIfweCanUseIndexedDb())
	{
		indexedDbDel(key);
	}
	else
	{
		inMemoryStore.delete(key);
	}
};
